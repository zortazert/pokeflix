# pokeflix

This is a script for make downloading the Pokemon TV series. You can download a whole series or just a single episode.

The script is called `pokeflix` because all episodes are found and downloaded from [pokeflix.tv](https://www.pokeflix.tv/). 

# Setup

- In order to install the script and other files, do a git clone on the repo: `git clone https://codeberg.org/zortazert/pokeflix`
- To actually download the episodes/tv series [yt-dlp](https://github.com/yt-dlp/yt-dlp) or [yt-dl](https://github.com/ytdl-org/youtube-dl) needs to be installed.
- Edit the [config file](pokeflix.conf) to suit your needs:

```ini
[CONFIG]
; This is telling the script to use pokeflix.tv, probably should be left the same
website = https://www.pokeflix.tv 
; Specify how yt-dlp or yt-dl can be accessed on your computer. 
downloader = yt-dlp
; The main folder where the pokemon episodes/tv series will be downloaded.
main_folder = ~/Downloads/Pokemon/
; Either 0 for just downloading usually or 1 for using the player.
use_player = 0
; Set a player on your system that can play website urls.
player = mpv
```

# Usage

- `-s` or `--series` Download a series
- `-e` or `--episode` Download or play an episode of a pokemon series
- `-m` or `--movies` Download or play a movie
- `-am` or `--all-movies` Download ALL movies
- `-S` or `--specials` Download a special
- `-aS` or `--all-specials` Download ALL specials
- `-g` or `--generation` Download a whole generation all of it's series
- `-dp` or `--dirplay` to help you pick from an ordered list an episode you downloaded to play in your player.
- `-f` or `--find` to search movies and series for some string

**NOTE**: Some specials are sort of spinoff episodes... not something well known like "Pokemon Origins", "Twilight Wings" and "Evolutions". All of those big ones are supported in the specials. However, not all use pokeflix for downloading. Right now watch.pokemon.tv is being used. In the event these specials go off of watch.pokemon.tv, YouTube could be used maybe? However, by then I hope pokeflix gets an update and has these specials.

# Similar scripts
- https://codeberg.org/zortazert/Python-Projects/src/branch/main/scrape/pokemon/pokevid.py tutorial to download directly from the pokemon website.

# See it in action!

[![Video explination](https://lbry2.vanwanet.com/speech/c065H93JqIiMuLdaTg8ABQGNsEjtfU2FDmCh1bRlzrxvkeyV4K:b)](https://tube.tchncs.de/w/qZVjaacc71qAm2oFUZ1E6m)

^ Click the image above to view the video on peertube or just click this link https://tube.tchncs.de/w/qZVjaacc71qAm2oFUZ1E6m

Also can be viewed on various lbry links:
- Librarian https://lbry.mutahar.rocks/@trueauracoral:a/pokeflixpyDownloadawholePokemonseries:7
- Odysee https://odysee.com/@trueauracoral:a/pokeflixpyDownloadawholePokemonseries:7
- LBRY URL lbry://@trueauracoral:a/pokeflixpyDownloadawholePokemonseries:7

# FAQ
## Why doesn't this use yt-dlp's python plugin?

The biggest problem with using that is you can't choose where it puts files downloaded. I have read the documentation and don't see a way how. Using yt-dlp or yt-dl on the command line you can easily tell where files need to go `yt-dlp -o "~/Downloads/%(title)s.%(ext)s" 'URL'` or even do something more complicated `cd ~/Downloads/ && yt-dlp URL`.

The second problem is more stuff you need to install to use this script. Right now all you need is yt-dlp or yt-dl and python. Optionally for downloading thumbnails you need to install python requests and if you want to watch, not download videos you need a video player. This way you can be up and running with pokeflix.py in as little time as possible. Rather than worrying about installing yt-dlp's python plugin `pip install yt-dlp`.

In the future it would be cool if I didn't need yt-dlp/yt-dl. I would need to figure out how to download the videos using the same extraction technique yt-dl/yt-dlp is using. Pokeflix or other sites aren't easy to scrape directly.

## What if pokeflix etc go down?

It's inevitable that websites like pokeflix will be shut down. At the end of the day some people don't like you getting sort of paid content for free on the clearnet from unnoficial sources. So if your worrying about the fact pokeflix will go down use this script to download what you need and have it saved on your computer to give to others. Spread it forward!

# Disclaimer

All episodes/TV series are owned by The Pokemon Company. Not me who made this script or [pokeflix.tv](https://www.pokeflix.tv/) providing them for free.

# Help is Appreciated!

I am not a great programmer, yet. Please write bug reports, suggestions for how to make the script better, and code improvements!

Because many of this script's users are running *nix operating systems while I am developing it for Microsoft Windows, I'll do my best to make sure everything functions on both systems. Please file reports or make contributions.

# Contact me

- Email: zortazert@matthewevan.xyz
- Matrix: @trueauracoral:tchncs.de
