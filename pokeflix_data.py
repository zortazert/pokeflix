series = '''Generation 8 (Galar Region)
(8) Pokemon: Ultimate Journeys
(8) Pokemon: Master Journeys
(8) Pokemon: Journeys
Generation 7 (Alola Region)
(7) Pokemon: Sun & Moon: Ultra Legend
(7) Pokemon: Sun & Moon: Ultra Adventure
(7) Pokemon: Sun & Moon
Generation 6 (Kalos Region)
(6) Pokemon: XYZ
(6) Pokemon: XY Kalos Quest
(6) Pokemon: XY
Generation 5 (Unova Region)
(5) Pokemon: BW Adventures in Unova
(5) Pokemon: BW Rival Destinies
(5) Pokemon: Black & White
Generation 4 (Sinnoh Region)
(4) Pokemon: DP Sinnoh League Victors
(4) Pokemon: DP Galactic Battles
(4) Pokemon: DP Battle Dimension
(4) Pokemon: Diamond and Pearl
Generation 3 (Hoenn Region)
(3) Pokemon: Battle Frontier
(3) Pokemon: Advanced Battle
(3) Pokemon: Advanced Challenge
(3) Pokemon: Advanced
Generation 2 (Johto Region)
(2) Pokemon: Master Quest
(2) Pokemon: Johto League Champions
(2) Pokemon: The Johto Journeys
Generation 1 (Kanto Region & Orange Islands)
(1) Pokemon: Orange Island Adventures
(1) Pokemon: Indigo League'''

movies = '''Generation 8 (Galar Region)
/static/thumbnails/movies/23.png
(8) Pokemon the Movie: Secrets of the Jungle
/v/pokemon-movie-secrets-of-jungle
Generation 7 (Alola Region)
/static/thumbnails/movies/22.png
(7) Pokemon Mewtwo Strikes Back: Evolution
/v/pokemon-movie-mewtwo-strikes-back-evolution
/static/thumbnails/movies/21.png
(7) Pokemon the Movie: The Power of Us
/v/pokemon-movie-the-power-of-us
/static/thumbnails/movies/20.png
(7) Pokemon the Movie: I Choose You!
/v/pokemon-movie-2017-i-choose-you
Generation 6 (Kalos Region)
/static/thumbnails/movies/19.png
(6) Pokemon the Movie: Volcanion and the Mechanical Marvel
/v/pokemon-movie-volcanion-mechanical-marvel
/static/thumbnails/movies/18.png
(6) Pokemon the Movie: Hoopa and the Clash of Ages
/v/pokemon-movie-hoopa-clash-of-ages
/static/thumbnails/movies/17.png
(6) Pokemon the Movie: Diancie and the Cocoon of Destruction
/v/pokemon-movie-diancie-cocoon-of-destruction
Generation 5 (Unova Region)
/static/thumbnails/movies/16.png
(5) Pokemon the Movie: Genesect and the Legend Awakened
/v/pokemon-movie-genesect-legend-awakened
/static/thumbnails/movies/15.png
(5) Pokemon the Movie: Kyurem VS. The Sword of Justice
/v/pokemon-movie-kyurem-vs-sword-of-justice
/static/thumbnails/movies/14.png
(5)Pokemon The Movie: White
/v/pokemon-movie-white-victini-zekrom
Generation 4 (Sinnoh Region)
/static/thumbnails/movies/13.png
(4) Pokemon: Zoroark: Master of Illusions
/v/pokemon-movie-zoroark-master-of-illusions
/static/thumbnails/movies/12.png
(4) Pokemon: Arceus and the Jewel of Life
/v/pokemon-movie-arceus-jewel-of-life
/static/thumbnails/movies/11.png
(4) Pokemon: Giratina and the Sky Warrior
/v/pokemon-movie-giratina-sky-warrior
/static/thumbnails/movies/10.png
(4) Pokemon: The Rise of Darkrai
/v/pokemon-movie-rise-of-darkrai
Generation 3 (Hoenn Region)
/static/thumbnails/movies/9.png
(3) Pokemon Ranger and the Temple of the Sea
/v/pokemon-movie-ranger-temple-of-sea
/static/thumbnails/movies/8.png
(3) Pokemon: Lucario and the Mystery of Mew
/v/pokemon-movie-lucario-mystery-of-mew
/static/thumbnails/movies/7.png
(3) Pokemon: Destiny Deoxys
/v/pokemon-movie-destiny-deoxys
/static/thumbnails/movies/6.png
(3) Pokemon: Jirachi: Wish Maker
/v/pokemon-movie-jirachi-wishmaker
Generation 2 (Johto Region)
/static/thumbnails/movies/5.png
(2) Pokemon Heroes
/v/pokemon-movie-heroes-laios-latias
/static/thumbnails/movies/4.png
(2) Pokemon 4Ever
/v/pokemon-movie-4ever-celebi-voice-of-forest
/static/thumbnails/movies/3.png
(2) Pokemon 3: The Movie
/v/pokemon-movie-3-spell-of-unown
Generation 1 (Kanto Region & Orange Islands)
/static/thumbnails/movies/2.png
(1) Pokemon: The Movie 2000
/v/pokemon-movie-2000-power-of-one
/static/thumbnails/movies/1.png
(1) Pokemon: The First Movie
/v/pokemon-movie-mewtwo-strikes-back
'''

specials = '''Evolutions
Twilight Wings
Pokemon Generations
Mega Evolution Specials
Pokemon Origins
Pokemon Chronicles
Pikachu Shorts
Mystery Dungeon Specials
Others
'''

Others = '''
/static/thumbnails/0-specials/1.png
01 - Mewtwo Returns
/video/pokemon-special-mewtwo-returns
/static/thumbnails/0-specials/2.png
02 - The Mastermind of Mirage Pokemon
/video/pokemon-special-mastermind-of-mirage-pokemon
/static/thumbnails/0-specials/3.png
03 - Mewtwo: Prologue to Awakening
/video/pokemon-special-mewtwo-prologue-to-awakening
/static/thumbnails/0-specials/4.png
04 - Slowking's Day
/video/pokemon-special-slowkings day
/static/thumbnails/0-specials/5.png
05 - Pokemon Ranger: Guardian Signs (Part 1 - 2)
/video/pokemon-special-ranger-guardian-signs
/static/thumbnails/0-specials/6.png
06 - Hikari - Setting Off on a New Journey!
/video/pokemon-special-hikari-setting-off-on-a-new-journey
/static/thumbnails/0-specials/7.png
07 - Nibi Gym - The Greatest Crisis Ever!
/video/pokemon-special-nibi-gym-the-greatest-crisis-ever
/static/thumbnails/0-specials/8.png
08 - Diancie: Princess of the Diamond Domain
/video/pokemon-special-diancie-princess-of-diamond-domain
/static/thumbnails/0-specials/9.png
09 - Hoopa the Mischief Pokemon
/video/pokemon-special-hoopa-mischief-pokemon
/static/thumbnails/0-specials/10.png
10 - Dent and Takeshi! Gyarados's Imperial Rage!
/video/pokemon-special-dent-takeshi-gyarados-imperial-rage
/static/thumbnails/0-specials/11.png
11 - Iris vs Ibuki! The Road to Become a Dragon Master!
/video/pokemon-special-iris-vs-ibuki-road-to-become-dragon-master
/static/thumbnails/0-specials/12.png
12 - The Strongest Duo! Citron and Dent!!
/video/pokemon-special-the-strongest-duo-citron-and-dent
/static/thumbnails/0-specials/13.png
13 - Detective Pikachu
/video/pokemon-special-detective-pikachu
/static/thumbnails/0-specials/14.png
14 - Poketoon: Zuruggu and Mimikkyu
/video/pokemon-special-poketoon
/static/thumbnails/0-specials/15.png
15 - Happy Birthday To You
/video/pokemon-special-happy-birthday-to-you
'''

Mystery_Dungeon_Specials = '''
/static/thumbnails/0-mystery-dungeon/1.png
01 - Team Go-Getters out of the Gate
/video/mystery-dungeon-team-go-getters-out-gate
/static/thumbnails/0-mystery-dungeon/2.png
02 - Explorers of Time & Darkness
/video/mystery-dungeon-explorers-time-darkness
/static/thumbnails/0-mystery-dungeon/3.png
03 - Explorers of Sky - Beyond Time & Darkness
/video/mystery-dungeon-explorers-sky-beyond-time-darkness
'''

Pikachu_Shorts = '''
/static/thumbnails/0-pikachu-shorts/1.png
01 - Pikachu's Vacation
/video/pikachu-short-pikachus-vacation
/static/thumbnails/0-pikachu-shorts/2.png
02 - Pikachu's Rescue Adventure
/video/pikachu-short-pikachus-rescue-adventure
/static/thumbnails/0-pikachu-shorts/3.png
03 - Pikachu's Winter Vacation 2
/video/pikachu-short-pikachus-winter-vacation-2
/static/thumbnails/0-pikachu-shorts/4.png
04 - Stantler's Little Helpers
/video/pikachu-short-stantlers-little-helpers
/static/thumbnails/0-pikachu-shorts/5.png
05 - Pikachu & Pichu
/video/pikachu-short-pikachu-and-pichu
/static/thumbnails/0-pikachu-shorts/6.png
06 - Pikachu's PikaBoo
/video/pikachu-short-pikachus-pikaboo
/static/thumbnails/0-pikachu-shorts/7.png
07 - Camp Pikachu
/video/pikachu-short-camp-pikachu
/static/thumbnails/0-pikachu-shorts/8.png
08 - Gotta Dance!
/video/pikachu-short-gotta-dance
/static/thumbnails/0-pikachu-shorts/9.png
09 - Pikachu's Summer Festival
/video/pikachu-short-pikachus-summer-fetival
/static/thumbnails/0-pikachu-shorts/10.png
10 - Pikachu's Ghost Carnival
/video/pikachu-short-pikachus-ghost-carnival
/static/thumbnails/0-pikachu-shorts/11.png
11 - Pikachu's Island Adventure
/video/pikachu-short-pikachus-island-adventure
/static/thumbnails/0-pikachu-shorts/12.png
12 - Pikachu's Exploration Club
/video/pikachu-short-pikachus-exploration-club
/static/thumbnails/0-pikachu-shorts/13.png
13 - Pikachu's Ice Adventure
/video/pikachu-short-pikachus-ice-adventure
/static/thumbnails/0-pikachu-shorts/14.png
14 - Pikachu's Sparkle Search
/video/pikachu-short-pikachus-sparkle-search
/static/thumbnails/0-pikachu-shorts/15.png
15 - Pikachu's Strange Wonder Adventure
/video/pikachu-short-pikachus-strange-wonder-adventure
/static/thumbnails/0-pikachu-shorts/16.png
16 - Pikachu's Summer Bridge Story
/video/pikachu-short-pikachus-summer-bridge-story
/static/thumbnails/0-pikachu-shorts/17.png
17 - Sing Meloetta: Search for the Rinka Berries
/video/pikachu-short-sing-meloetta-search-for-the-rinka-berries
/static/thumbnails/0-pikachu-shorts/18.png
18 - Meloetta's Moonlight Serenade
/video/pikachu-short-meloettas-moonlight-serenade
/static/thumbnails/0-pikachu-shorts/19.png
19 - Eevee & Friends
/video/pikachu-short-eevee-and-friends
/static/thumbnails/0-pikachu-shorts/20.png
20 - Pikachu, What's This Key?
/video/pikachu-short-whats-this-key
/static/thumbnails/0-pikachu-shorts/21.png
21 - Pikachu and the Pokemon Music Squad
/video/pikachu-short-pokemon-music-squad
'''

Pokemon_Chronicles = '''
/static/thumbnails/0-chronicles/1.png
01 - The Legend of Thunder! (Part 1 - 3)
/video/pokemon-chronicles-legend-of-thunder
/static/thumbnails/0-chronicles/4.png
04 - Pikachu's Winter Vacation (Part 1 - 2)
/video/pokemon-chronicles-pikachus-winter-vacation-1
/static/thumbnails/0-chronicles/5.png
05 - A Family That Battles Together Stays Together!
/video/pokemon-chronicles-family-that-battles-together-stays-together
/static/thumbnails/0-chronicles/6.png
06 - Cerulean Blues
/video/pokemon-chronicles-cerulean-blues
/static/thumbnails/0-chronicles/7.png
07 - We're No Angels!
/video/pokemon-chronicles-were-no-angels
/static/thumbnails/0-chronicles/8.png
08 - Showdown at the Oak Corral
/video/pokemon-chronicles-showdown-at-oak-corral
/static/thumbnails/0-chronicles/9.png
09 - The Blue Badge of Courage
/video/pokemon-chronicles-blue-badge-of-courage
/static/thumbnails/0-chronicles/10.png
10 - Oaknapped!
/video/pokemon-chronicles-oaknapped
/static/thumbnails/0-chronicles/11.png
11 - A Date With Delcatty
/video/pokemon-chronicles-date-with-delcatty
/static/thumbnails/0-chronicles/12.png
12 - Celebi and Joy!
/video/pokemon-chronicles-celebi-and-joy
/static/thumbnails/0-chronicles/13.png
13 - Training Daze
/video/pokemon-chronicles-training-daze
/static/thumbnails/0-chronicles/14.png
14 - Journey to the Starting Line!
/video/pokemon-chronicles-journey-to-starting-line
/static/thumbnails/0-chronicles/15.png
15 - Putting the Air Back in Aerodactyl!
/video/pokemon-chronicles-putting-air-back-in-aerodactyl
/static/thumbnails/0-chronicles/16.png
16 - Luvdisc is a Many Splendored Thing!
/video/pokemon-chronicles-luvdisc-many-splendored-thing
/static/thumbnails/0-chronicles/17.png
17 - Those Darn Electabuzz!
/video/pokemon-chronicles-those-darn-electabuzz
/static/thumbnails/0-chronicles/18.png
18 - The Search for the Legend
/video/pokemon-chronicles-search-for-legend
/static/thumbnails/0-chronicles/19.png
19 - Of Meowth and Pokemon
/video/pokemon-chronicles-of-meowth-and-pokemon
/static/thumbnails/0-chronicles/20.png
20 - Trouble in Big Town
/video/pokemon-chronicles-trouble-in-big-town
/static/thumbnails/0-chronicles/21.png
21 - Big Meowth, Little Dreams
/video/pokemon-chronicles-big-meowth-little-dreams
/static/thumbnails/0-chronicles/22.png
22 - Pikachu's Winter Vacation (Part 3 - 4)
/video/pokemon-chronicles-pikachus-winter-vacation-2
'''

Pokemon_Origins = '''
/static/thumbnails/0-origins/1.png
01 - Episode 1 - File 1: Red
/video/pokemon-origins-01-red
/static/thumbnails/0-origins/2.png
02 - Episode 2 - File 2: Cubone
/video/pokemon-origins-02-cubone
/static/thumbnails/0-origins/3.png
03 - Episode 3 - File 3: Giovanni
/video/pokemon-origins-03-giovanni
/static/thumbnails/0-origins/4.png
04 - Episode 4 - File 4: Charizard
/video/pokemon-origins-04-charizard
'''

Mega_Evolution_Specials = '''
/static/thumbnails/0-mega-evo-specials/1.png
01 - Mega Evolution Special I
/video/pokemon-special-mega-evolution-i
/static/thumbnails/0-mega-evo-specials/2.png
02 - Mega Evolution Special II
/video/pokemon-special-mega-evolution-ii
/static/thumbnails/0-mega-evo-specials/3.png
03 - Mega Evolution Special III
/video/pokemon-special-mega-evolution-iii
/static/thumbnails/0-mega-evo-specials/4.png
04 - Mega Evolution Special IV
/video/pokemon-special-mega-evolution-iv
'''

Pokemon_Generations = '''
/static/thumbnails/0-generations/1.png
01 - The Adventure
/video/pokemon-generations-the-adventure
/static/thumbnails/0-generations/2.png
02 - The Chase
/video/pokemon-generations-the-chase
/static/thumbnails/0-generations/3.png
03 - The Challenger
/video/pokemon-generations-the-challenger
/static/thumbnails/0-generations/4.png
04 - The Lake of Rage
/video/pokemon-generations-the-lake-of-rage
/static/thumbnails/0-generations/5.png
05 - The Legacy
/video/pokemon-generations-the-legacy
/static/thumbnails/0-generations/6.png
06 - The Reawakening
/video/pokemon-generations-the-reawakening
/static/thumbnails/0-generations/7.png
07 - The Vision
/video/pokemon-generations-the-vision
/static/thumbnails/0-generations/8.png
08 - The Cavern
/video/pokemon-generations-the-cavern
/static/thumbnails/0-generations/9.png
09 - The Scoop
/video/pokemon-generations-the-scoop
/static/thumbnails/0-generations/10.png
10 - The Old Chateau
/video/pokemon-generations-the-old-chateau
/static/thumbnails/0-generations/11.png
11 - The New World
/video/pokemon-generations-the-new-world
/static/thumbnails/0-generations/12.png
12 - The Magma Stone
/video/pokemon-generations-the-magma-stone
/static/thumbnails/0-generations/13.png
13 - The Uprising
/video/pokemon-generations-the-uprising
/static/thumbnails/0-generations/14.png
14 - The Frozen World
/video/pokemon-generations-the-frozen-world
/static/thumbnails/0-generations/15.png
15 - The King Returns
/video/pokemon-generations-the-king-returns
/static/thumbnails/0-generations/16.png
16 - The Beauty Eternal
/video/pokemon-generations-the-beauty-eternal
/static/thumbnails/0-generations/17.png
17 - The Investigation
/video/pokemon-generations-the-investigation
/static/thumbnails/0-generations/18.png
18 - The Redemption
/video/pokemon-generations-the-redemption
'''

# Pokeflix is missing 3 episodes that are free to get from
# Pokemon's OFFICIAL YouTube channel... OR even better because YouTube
# sucks: watch.pokemon.tv so listing those links instead of pokeflix
Twilight_Wings = '''
/static/thumbnails/0-twilight-wings/1.png
01 - Letter
/video/tw-letter
/static/thumbnails/0-twilight-wings/2.png
02 - Training
/video/tw-training
/static/thumbnails/0-twilight-wings/3.png
03 - Buddy
/video/tw-buddy
/static/thumbnails/0-twilight-wings/4.png
04 - Early-Evening Waves
/video/tw-early-evening-waves
/static/thumbnails/0-twilight-wings/5.png
05 - Assistant
/video/tw-assistant
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/twilight-wings/twilight_wings_ep06_ss01_360_206.jpg
06 - Moonlight (watch.pokemon.tv)
https://watch.pokemon.com/en-us/#/player?id=c45d24fbdee84091a3a2a29596b07ff8&channelId=pokemon-twilight-wings&cameFromHome=false
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/twilight-wings/twilight_wings_ep07_ss01_360_206.jpg
07 - Sky
https://watch.pokemon.com/en-us/#/player?id=3d863899d6d34d2cba0c0f8b50bbc771&channelId=pokemon-twilight-wings&cameFromHome=false
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/twilight-wings/twilight_wings_ep08_ss01_360_206.png
08 - Twilight Wings - The Gathering of Stars
https://watch.pokemon.com/en-us/#/player?id=f16c392ab43d44d19e279eb3d434f155&channelId=pokemon-twilight-wings&cameFromHome=false
'''

# The very new "Evolutions" series pokeflix doesn't have at all. So I
# am using data I got from watch.pokemon.tv
Evolutions = '''
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/evolutions/evolutions_ep01_ss01_360_206.jpg
01 - The Champion
https://watch.pokemon.com/en-us/#/player?id=ed67562cd0a94e14b8a136c93a325527&channelId=pokemon-evolutions&cameFromHome=false
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/evolutions/evolutions_ep02_ss01_360_206.png
02 - The Eclipse
https://watch.pokemon.com/en-us/#/player?id=9e2e057a3d5843f7a554c560699eb435&channelId=pokemon-evolutions&cameFromHome=false
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/evolutions/evolutions_ep03_ss01_360_206.png
03 - The Visionary
https://watch.pokemon.com/en-us/#/player?id=edd19c8d25e242919be4ef680d406780&channelId=pokemon-evolutions&cameFromHome=false
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/evolutions/evolutions_ep04_ss01_360_206.png
04 - The Plan
https://watch.pokemon.com/en-us/#/player?id=d003af6b82ab48768c66ec0e96618cf9&channelId=pokemon-evolutions&cameFromHome=false
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/evolutions/evolutions_ep05_ss01_360_206.png
05 - The Rival
https://watch.pokemon.com/en-us/#/player?id=1848585bc0f0492ba9a0586e1f69f7e6&channelId=pokemon-evolutions&cameFromHome=false
src="https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/evolutions/evolutions_ep06_ss01_360_206.jpg"
06 - The Wish
https://watch.pokemon.com/en-us/#/player?id=4b38bfe7f0bd48dd8faf4ebcb9963907&channelId=pokemon-evolutions&cameFromHome=false
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/evolutions/evolutions_ep07_ss01_360_206.jpg
07 - The Show
https://watch.pokemon.com/en-us/#/player?id=2ef0859954a54d62b332e4b8bde3a564&channelId=pokemon-evolutions&cameFromHome=false
https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original/evolutions/evolutions_ep08_ss01_360_206.jpg
08 - The Discovery
https://watch.pokemon.com/en-us/#/player?id=c02f0b92d1f947c9859a9b5ae5e72c6a&channelId=pokemon-evolutions&cameFromHome=false
'''

Indigo_League = '''/static/thumbnails/01-indigo-league/1.png
01 - Pokemon - I Choose You!
/video/01-pokemon-i-choose-you
/static/thumbnails/01-indigo-league/2.png
02 - Pokemon Emergency!
/video/01-pokemon-emergency
/static/thumbnails/01-indigo-league/3.png
03 - Ash Catches a Pokemon
/video/01-ash-catches-a-pokemon
/static/thumbnails/01-indigo-league/4.png
04 - Challenge of the Samurai
/video/01-challenge-of-the-samurai
/static/thumbnails/01-indigo-league/5.png
05 - Showdown in Pewter City
/video/01-showdown-in-pewter-city
/static/thumbnails/01-indigo-league/6.png
06 - Clefairy and the Moon Stone
/video/01-clefairy-and-the-moon-stone
/static/thumbnails/01-indigo-league/7.png
07 - The Water Flowers of Cerulean City
/video/01-water-flowers-of-cerulean-city
/static/thumbnails/01-indigo-league/8.png
08 - The Path to the Pokemon League
/video/01-path-to-the-pokemon-league
/static/thumbnails/01-indigo-league/9.png
09 - The School of Hard Knocks
/video/01-school-of-hard-knocks
/static/thumbnails/01-indigo-league/10.png
10 - Bulbasaur and the Hidden Village
/video/01-bulbasaur-and-the-hidden-village
/static/thumbnails/01-indigo-league/11.png
11 - Charmander - The Stray Pokemon
/video/01-charmander-the-stray-pokemon
/static/thumbnails/01-indigo-league/12.png
12 - Here Comes the Squirtle Squad
/video/01-here-comes-the-squirtle-squad
/static/thumbnails/01-indigo-league/13.png
13 - Mystery at the Lighthouse
/video/01-mystery-at-the-lighthouse
/static/thumbnails/01-indigo-league/14.png
14 - Electric Shock Showdown
/video/01-electric-shock-showdown
/static/thumbnails/01-indigo-league/15.png
15 - Battle Aboard the St. Anne
/video/01-battle-aboard-the-st-anne
/static/thumbnails/01-indigo-league/16.png
16 - Pokemon Shipwreck
/video/01-pokemon-shipwreck
/static/thumbnails/01-indigo-league/17.png
17 - Island of the Giant Pokemon
/video/01-island-of-the-giant-pokemon
/static/thumbnails/01-indigo-league/18.png
18 - Beauty and the Beach
/video/01-beauty-and-the-beach
/static/thumbnails/01-indigo-league/19.png
19 - Tentacool & Tentacruel
/video/01-tentacool-and-tentacruel
/static/thumbnails/01-indigo-league/20.png
20 - The Ghost of Maiden's Peak
/video/01-the-ghost-of-maidens-peak
/static/thumbnails/01-indigo-league/21.png
21 - Bye Bye Butterfree
/video/01-bye-bye-butterfree
/static/thumbnails/01-indigo-league/22.png
22 - Abra and the Psychic Showdown
/video/01-abra-and-the-psychic-showdown
/static/thumbnails/01-indigo-league/23.png
23 - The Tower of Terror
/video/01-tower-of-terror
/static/thumbnails/01-indigo-league/24.png
24 - Haunter Versus Kadabra
/video/01-haunter-versus-kadabra
/static/thumbnails/01-indigo-league/25.png
25 - Primeape Goes Bananas
/video/01-primeape-goes-bananas
/static/thumbnails/01-indigo-league/26.png
26 - Pokemon Scent-sation
/video/01-pokemon-scent-sation
/static/thumbnails/01-indigo-league/27.png
27 - Hypno's Naptime
/video/01-hypnos-naptime
/static/thumbnails/01-indigo-league/28.png
28 - Pokemon Fashion Flash
/video/01-pokemon-fashion-flash
/static/thumbnails/01-indigo-league/29.png
29 - The Punchy Pokemon
/video/01-the-punchy-pokemon
/static/thumbnails/01-indigo-league/30.png
30 - Sparks Fly for Magnemite
/video/01-sparks-fly-for-magnemite
/static/thumbnails/01-indigo-league/31.png
31 - Dig Those Diglett!
/video/01-dig-those-diglett
/static/thumbnails/01-indigo-league/32.png
32 - The Ninja Poke-Showdown
/video/01-the-ninja-poke-showdown
/static/thumbnails/01-indigo-league/33.png
33 - The Flame Pokemon-athon!
/video/01-the-flame-pokemon-athon
/static/thumbnails/01-indigo-league/34.png
34 - The Kangaskhan Kid
/video/01-the-kangaskhan-kid
/static/thumbnails/01-indigo-league/35.png
35 - The Legend of Dratini
/video/01-the-legend-of-dratini
/static/thumbnails/01-indigo-league/36.png
36 - The Bridge Bike Gang
/video/01-the-bridge-bike-gang
/static/thumbnails/01-indigo-league/37.png
37 - Ditto's Mysterious Mansion
/video/01-dittos-mysterious-mansion
/static/thumbnails/01-indigo-league/38.png
38 - Electric Soldier Porygon
/video/01-electric-soldier-porygon
/static/thumbnails/01-indigo-league/39.png
39 - Pikachu's Goodbye
/video/01-pikachus-goodbye
/static/thumbnails/01-indigo-league/40.png
40 - Holiday Hi-Jynx
/video/01-holiday-hi-jynx
/static/thumbnails/01-indigo-league/41.png
41 - Snow Way Out!
/video/01-snow-way-out
/static/thumbnails/01-indigo-league/42.png
42 - The Battling Eevee Brothers
/video/01-the-battling-eevee-brothers
/static/thumbnails/01-indigo-league/43.png
43 - Wake Up Snorlax!
/video/01-wake-up-snorlax
/static/thumbnails/01-indigo-league/44.png
44 - Showdown at Dark City
/video/01-showdown-at-dark-city
/static/thumbnails/01-indigo-league/45.png
45 - The March of the Exeggutor Squad
/video/01-march-of-the-exeggutor-squad
/static/thumbnails/01-indigo-league/46.png
46 - The Problem with Paras
/video/01-the-problem-with-paras
/static/thumbnails/01-indigo-league/47.png
47 - The Song of Jigglypuff
/video/01-the-song-of-jigglypuff
/static/thumbnails/01-indigo-league/48.png
48 - Attack of the Prehistoric Pokemon
/video/01-attack-of-the-prehistoric-pokemon
/static/thumbnails/01-indigo-league/49.png
49 - A Chansey Operation
/video/01-a-chansey-operation
/static/thumbnails/01-indigo-league/50.png
50 - Holy Matrimony!
/video/01-holy-matrimony
/static/thumbnails/01-indigo-league/51.png
51 - So Near, Yet So Farfetch'd
/video/01-so-near-yet-so-farfetchd
/static/thumbnails/01-indigo-league/52.png
52 - Who Gets to Keep Togepi?
/video/01-who-gets-to-keep-togepi
/static/thumbnails/01-indigo-league/53.png
53 - Bulbasaur's Mysterious Garden
/video/01-bulbasaurs-mysterious-garden
/static/thumbnails/01-indigo-league/54.png
54 - Princess vs. Princess
/video/01-princess-vs-princess
/static/thumbnails/01-indigo-league/55.png
55 - The Purr-fect Hero
/video/01-the-purr-fect-hero
/static/thumbnails/01-indigo-league/56.png
56 - The Case of the K-9 Caper!
/video/01-the-case-of-the-k-9-caper
/static/thumbnails/01-indigo-league/57.png
57 - Pokemon Paparazzi
/video/01-pokemon-paparazzi
/static/thumbnails/01-indigo-league/58.png
58 - The Ultimate Test
/video/01-the-ultimate-test
/static/thumbnails/01-indigo-league/59.png
59 - The Breeding Center Secret
/video/01-the-breeding-center-secret
/static/thumbnails/01-indigo-league/60.png
60 - Riddle Me This
/video/01-riddle-me-this
/static/thumbnails/01-indigo-league/61.png
61 - Volcanic Panic
/video/01-volcanic-panic
/static/thumbnails/01-indigo-league/62.png
62 - Beach Blank-Out Blastoise
/video/01-beach-blank-out-blastoise
/static/thumbnails/01-indigo-league/63.png
63 - The Misty Mermaid
/video/01-the-misty-mermaid
/static/thumbnails/01-indigo-league/64.png
64 - Clefairy Tales
/video/01-clefairy-tales
/static/thumbnails/01-indigo-league/65.png
65 - The Battle of the Badge
/video/01-the-battle-of-the-badge
/static/thumbnails/01-indigo-league/66.png
66 - It's Mr. Mime Time
/video/01-its-mr-mime-time
/static/thumbnails/01-indigo-league/67.png
67 - Showdown at the Po-ke Corral
/video/01-showdown-at-the-po-ke-corral
/static/thumbnails/01-indigo-league/68.png
68 - The Evolution Solution
/video/01-the-evolution-solution
/static/thumbnails/01-indigo-league/69.png
69 - The Pi-Kahuna
/video/01-the-pi-kahuna
/static/thumbnails/01-indigo-league/70.png
70 - Make Room for Gloom
/video/01-make-room-for-gloom
/static/thumbnails/01-indigo-league/71.png
71 - Lights, Camera, Quack-tion
/video/01-lights-camera-quack-tion
/static/thumbnails/01-indigo-league/72.png
72 - Go West Young Meowth
/video/01-go-west-young-meowth
/static/thumbnails/01-indigo-league/73.png
73 - To Master the Onixpected!
/video/01-to-master-the-onixpected
/static/thumbnails/01-indigo-league/74.png
74 - The Ancient Puzzle of Pokemopolis
/video/01-the-ancient-puzzle-of-pokemopolis
/static/thumbnails/01-indigo-league/75.png
75 - Bad to the Bone
/video/01-bad-to-the-bone
/static/thumbnails/01-indigo-league/76.png
76 - All Fired Up!
/video/01-all-fired-up
/static/thumbnails/01-indigo-league/77.png
77 - Round One - Begin!
/video/01-round-one-begin
/static/thumbnails/01-indigo-league/78.png
78 - Fire and Ice
/video/01-fire-and-ice
/static/thumbnails/01-indigo-league/79.png
79 - The Fourth Round Rumble
/video/01-the-fourth-round-rumble
/static/thumbnails/01-indigo-league/80.png
80 - A Friend In Deed
/video/01-a-friend-in-deed
/static/thumbnails/01-indigo-league/81.png
81 - Friend and Foe Alike
/video/01-friend-and-foe-alike
/static/thumbnails/01-indigo-league/82.png
82 - Friends to the End
/video/01-friends-to-the-end'''

Orange_Island_Adventures = '''/static/thumbnails/02-orange-islands/1.png
01 - Pallet Party Panic
/video/02-pallet-party-panic
/static/thumbnails/02-orange-islands/2.png
02 - A Scare in the Air
/video/02-a-scare-in-the-air
/static/thumbnails/02-orange-islands/3.png
03 - Pokeball Peril
/video/02-pokeball-peril
/static/thumbnails/02-orange-islands/4.png
04 - The Lost Lapras
/video/02-the-lost-lapras
/static/thumbnails/02-orange-islands/5.png
05 - Fit to be Tide
/video/02-fit-to-be-tide
/static/thumbnails/02-orange-islands/6.png
06 - Pikachu Re-Volts
/video/02-pikachu-re-volts
/static/thumbnails/02-orange-islands/7.png
07 - The Crystal Onix
/video/02-the-crystal-onix
/static/thumbnails/02-orange-islands/8.png
08 - In the Pink
/video/02-in-the-pink
/static/thumbnails/02-orange-islands/9.png
09 - Shell Shock!
/video/02-shell-shock
/static/thumbnails/02-orange-islands/10.png
10 - Stage Fight!
/video/02-stage-fight
/static/thumbnails/02-orange-islands/11.png
11 - Bye Bye Psyduck
/video/02-bye-bye-psyduck
/static/thumbnails/02-orange-islands/12.png
12 - The Joy of Pokemon
/video/02-the-joy-of-pokemon
/static/thumbnails/02-orange-islands/13.png
13 - Navel Maneuvers
/video/02-navel-maneuvers
/static/thumbnails/02-orange-islands/14.png
14 - Snack Attack
/video/02-snack-attack
/static/thumbnails/02-orange-islands/15.png
15 - A Shipful of Shivers
/video/02-a-shipful-of-shivers
/static/thumbnails/02-orange-islands/16.png
16 - Meowth Rules!
/video/02-meowth-rules
/static/thumbnails/02-orange-islands/17.png
17 - Tracey Gets Bugged
/video/02-tracey-gets-bugged
/static/thumbnails/02-orange-islands/18.png
18 - A Way Off Day Off
/video/02-a-way-off-day-off
/static/thumbnails/02-orange-islands/19.png
19 - The Mandarin Island Miss Match
/video/02-the-mandarin-island-miss-match
/static/thumbnails/02-orange-islands/20.png
20 - Wherefore Art Thou, Pokemon?
/video/02-wherefore-art-thou-pokemon
/static/thumbnails/02-orange-islands/21.png
21 - Get Along, Little Pokemon
/video/02-get-along-little-pokemon
/static/thumbnails/02-orange-islands/22.png
22 - The Mystery Menace
/video/02-the-mystery-menace
/static/thumbnails/02-orange-islands/23.png
23 - Misty Meets Her Match
/video/02-misty-meets-her-match
/static/thumbnails/02-orange-islands/24.png
24 - Bound For Trouble
/video/02-bound-for-trouble
/static/thumbnails/02-orange-islands/25.png
25 - Charizard Chills
/video/02-charizard-chills
/static/thumbnails/02-orange-islands/26.png
26 - The Pokemon Water War
/video/02-the-pokemon-water-war
/static/thumbnails/02-orange-islands/27.png
27 - Pokemon Food Fight!
/video/02-pokemon-food-fight
/static/thumbnails/02-orange-islands/28.png
28 - Pokemon Double Trouble
/video/02-pokemon-double-trouble
/static/thumbnails/02-orange-islands/29.png
29 - The Wacky Watcher!
/video/02-the-wacky-watcher
/static/thumbnails/02-orange-islands/30.png
30 - The Stun Spore Detour
/video/02-the-stun-spore-detour
/static/thumbnails/02-orange-islands/31.png
31 - Hello, Pummelo!
/video/02-hello-pummelo
/static/thumbnails/02-orange-islands/32.png
32 - Enter The Dragonite
/video/02-enter-the-dragonite
/static/thumbnails/02-orange-islands/33.png
33 - Viva Las Lapras
/video/02-viva-las-lapras
/static/thumbnails/02-orange-islands/34.png
34 - The Underground Round Up
/video/02-the-underground-round-up
/static/thumbnails/02-orange-islands/35.png
35 - A Tent Situation
/video/02-a-tent-situation
/static/thumbnails/02-orange-islands/36.png
36 - The Rivalry Revival
/video/02-the-rivalry-revival'''

The_Johto_Journeys = '''/static/thumbnails/03-johto-journeys/1.png
01 - Don't Touch That 'dile
/video/03-dont-touch-that-dile
/static/thumbnails/03-johto-journeys/2.png
02 - The Double Trouble Header
/video/03-the-double-trouble-header
/static/thumbnails/03-johto-journeys/3.png
03 - A Sappy Ending
/video/03-a-sappy-ending
/static/thumbnails/03-johto-journeys/4.png
04 - Roll On, Pokemon!
/video/03-roll-on-pokemon
/static/thumbnails/03-johto-journeys/5.png
05 - Illusion Confusion!
/video/03-illusion-confusion
/static/thumbnails/03-johto-journeys/6.png
06 - Flower Power
/video/03-flower-power
/static/thumbnails/03-johto-journeys/7.png
07 - Spinarak Attack
/video/03-spinarak-attack
/static/thumbnails/03-johto-journeys/8.png
08 - Snubbull Snobbery
/video/03-snubbull-snobbery
/static/thumbnails/03-johto-journeys/9.png
09 - The Little Big Horn
/video/03-the-little-big-horn
/static/thumbnails/03-johto-journeys/10.png
10 - The Chikorita Rescue
/video/03-the-chikorita-rescue
/static/thumbnails/03-johto-journeys/11.png
11 - Once in a Blue Moon
/video/03-once-in-a-blue-moon
/static/thumbnails/03-johto-journeys/12.png
12 - The Whistle Stop
/video/03-the-whistle-stop
/static/thumbnails/03-johto-journeys/13.png
13 - Ignorance is Blissey
/video/03-ignorance-is-blissey
/static/thumbnails/03-johto-journeys/14.png
14 - A Bout With Sprout
/video/03-a-bout-with-sprout
/static/thumbnails/03-johto-journeys/15.png
15 - Fighting Flyer with Fire
/video/03-fighting-flyer-with-fire
/static/thumbnails/03-johto-journeys/16.png
16 - For Crying Out Loud
/video/03-for-crying-out-loud
/static/thumbnails/03-johto-journeys/17.png
17 - Tanks a Lot!
/video/03-tanks-a-lot
/static/thumbnails/03-johto-journeys/18.png
18 - Charizard's Burning Ambitions
/video/03-charizards-burning-ambitions
/static/thumbnails/03-johto-journeys/19.png
19 - Grin to Win!
/video/03-grin-to-win
/static/thumbnails/03-johto-journeys/20.png
20 - Chikorita's Big Upset
/video/03-chikoritas-big-upset
/static/thumbnails/03-johto-journeys/21.png
21 - Foul Weather Friends
/video/03-foul-weather-friends
/static/thumbnails/03-johto-journeys/22.png
22 - The Superhero Secret
/video/03-the superhero-secret
/static/thumbnails/03-johto-journeys/23.png
23 - Mild 'n Wooly
/video/03-mild-n-wooly
/static/thumbnails/03-johto-journeys/24.png
24 - Wired for Battle!
/video/03-wired-for-battle
/static/thumbnails/03-johto-journeys/25.png
25 - Good 'Quil Hunting
/video/03-good-quil-hunting
/static/thumbnails/03-johto-journeys/26.png
26 - A Shadow of a Drought
/video/03-a-shadow-of-a-drought
/static/thumbnails/03-johto-journeys/27.png
27 - Going Apricorn!
/video/03-going-apricorn
/static/thumbnails/03-johto-journeys/28.png
28 - Gettin' The Bugs Out
/video/03-getting-the-bugs-out
/static/thumbnails/03-johto-journeys/29.png
29 - A Farfetch'd Tale
/video/03-a-farfetchd-tale
/static/thumbnails/03-johto-journeys/30.png
30 - Tricks of the Trade
/video/03-tricks-of-the-trade
/static/thumbnails/03-johto-journeys/31.png
31 - The Fire-ing Squad!
/video/03-the-fire-ing-squad
/static/thumbnails/03-johto-journeys/32.png
32 - No Big Woop!
/video/03-no-big-woop
/static/thumbnails/03-johto-journeys/33.png
33 - Tunnel Vision
/video/03-tunnel-vision
/static/thumbnails/03-johto-journeys/34.png
34 - Hour of the Houndour
/video/03-hour-of-the-houndour
/static/thumbnails/03-johto-journeys/35.png
35 - The Totodile Duel
/video/03-the-totodile-duel
/static/thumbnails/03-johto-journeys/36.png
36 - Hot Matches!
/video/03-hot-matches
/static/thumbnails/03-johto-journeys/37.png
37 - Love, Totodile Style
/video/03-love-totodile-style
/static/thumbnails/03-johto-journeys/38.png
38 - Fowl Play!
/video/03-fowl-play
/static/thumbnails/03-johto-journeys/39.png
39 - Forest Grumps
/video/03-forest-grumps
/static/thumbnails/03-johto-journeys/40.png
40 - The Psychic Sidekicks!
/video/03-the-psychic-sidekicks
/static/thumbnails/03-johto-journeys/41.png
41 - The Fortune Hunters
/video/03-the-fortune-hunters'''

Johto_League_Champions = '''
/static/thumbnails/04-johto-league/1.png
01 - A Goldenrod Opportunity
/video/04-a-goldenrod-opportunity
/static/thumbnails/04-johto-league/2.png
02 - A Dairy Tale Ending
/video/04-a-dairy-tale-ending
/static/thumbnails/04-johto-league/3.png
03 - Air Time!
/video/04-air-time
/static/thumbnails/04-johto-league/4.png
04 - The Bug Stops Here
/video/04-the-bug-stops-here
/static/thumbnails/04-johto-league/5.png
05 - Type Casting
/video/04-type-casting
/static/thumbnails/04-johto-league/6.png
06 - Fossil Fools
/video/04-fossil-fools
/static/thumbnails/04-johto-league/7.png
07 - Carrying On!
/video/04-carrying-on
/static/thumbnails/04-johto-league/8.png
08 - Hassle in the Castle
/video/04-hassle-in-the-castle
/static/thumbnails/04-johto-league/9.png
09 - Two Hits and a Miss
/video/04-two-hits-and-a-miss
/static/thumbnails/04-johto-league/10.png
10 - A Hot Water Battle
/video/04-a-hot-water-battle
/static/thumbnails/04-johto-league/11.png
11 - Hook, Line, and Stinker
/video/04-hook-line-and-stinker
/static/thumbnails/04-johto-league/12.png
12 - Beauty and the Breeder
/video/04-beauty-and-the-breeder
/static/thumbnails/04-johto-league/13.png
13 - A Better Pill to Swallow
/video/04-a-better-pill-to-swallow
/static/thumbnails/04-johto-league/14.png
14 - Power Play!
/video/04-power-play
/static/thumbnails/04-johto-league/15.png
15 - Mountain Time
/video/04-mountain-time
/static/thumbnails/04-johto-league/16.png
16 - Wobbu-Palooza!
/video/04-wobbu-palooza
/static/thumbnails/04-johto-league/17.png
17 - Imitation Confrontation
/video/04-imitation-confrontation
/static/thumbnails/04-johto-league/18.png
18 - The Trouble With Snubbull
/video/04-the-trouble-with-snubbull
/static/thumbnails/04-johto-league/19.png
19 - Ariados Amigos
/video/04-ariados-amigos
/static/thumbnails/04-johto-league/20.png
20 - Wings 'N' Things
/video/04-wings-n-things
/static/thumbnails/04-johto-league/21.png
21 - The Grass Route
/video/04-the-grass-route
/static/thumbnails/04-johto-league/22.png
22 - The Apple Corp!
/video/04-the-apple-corp
/static/thumbnails/04-johto-league/23.png
23 - Houndoom's Special Delivery
/video/04-houndooms-special-delivery
/static/thumbnails/04-johto-league/24.png
24 - A Ghost of a Chance
/video/04-a-ghost-of-a-chance
/static/thumbnails/04-johto-league/25.png
25 - From Ghost to Ghost
/video/04-from-ghost-to-ghost
/static/thumbnails/04-johto-league/26.png
26 - Trouble's Brewing
/video/04-troubles-brewing
/static/thumbnails/04-johto-league/27.png
27 - All That Glitters!
/video/04-all-that-glitters
/static/thumbnails/04-johto-league/28.png
28 - The Light Fantastic
/video/04-the-light-fantastic
/static/thumbnails/04-johto-league/29.png
29 - UnBEARable
/video/04-unbearable
/static/thumbnails/04-johto-league/30.png
30 - Moving Pictures
/video/04-moving-pictures
/static/thumbnails/04-johto-league/31.png
31 - Spring Fever
/video/04-spring-fever
/static/thumbnails/04-johto-league/32.png
32 - Freeze Frame
/video/04-freeze-frame
/static/thumbnails/04-johto-league/33.png
33 - The Stolen Stones!
/video/04-the-stolen-stones
/static/thumbnails/04-johto-league/34.png
34 - The Dunsparce Deception
/video/04-the-dunsparce-deception
/static/thumbnails/04-johto-league/35.png
35 - The Wayward Wobbuffet
/video/04-the-wayward-wobbuffet
/static/thumbnails/04-johto-league/36.png
36 - Sick Daze
/video/04-sick-daze
/static/thumbnails/04-johto-league/37.png
37 - Ring Masters
/video/04-ring-masters
/static/thumbnails/04-johto-league/38.png
38 - The Poke Spokesman
/video/04-the-poke-spokesman
/static/thumbnails/04-johto-league/39.png
39 - Control Freak!
/video/04-control-freak
/static/thumbnails/04-johto-league/40.png
40 - The Art Of Pokemon
/video/04-the-art-of-pokemon
/static/thumbnails/04-johto-league/41.png
41 - The Heartbreak of Brock
/video/04-the-heartbreak-of-brock
/static/thumbnails/04-johto-league/42.png
42 - Current Events
/video/04-current-events
/static/thumbnails/04-johto-league/43.png
43 - Turning Over A New Bayleef
/video/04-turning-over-a-new-bayleef
/static/thumbnails/04-johto-league/44.png
44 - Doin' What Comes Natu-rally
/video/04-doin-what-comes-natu-rally
/static/thumbnails/04-johto-league/45.png
45 - The Big Balloon Blow-Up
/video/04-the-big-balloon-blow-up
/static/thumbnails/04-johto-league/46.png
46 - The Screen Actor's Guilt
/video/04-the-screen-actors-guilt
/static/thumbnails/04-johto-league/47.png
47 - Right on, Rhydon!
/video/04-right-on-rhydon
/static/thumbnails/04-johto-league/48.png
48 - The Kecleon Caper
/video/04-the-kecleon-caper
/static/thumbnails/04-johto-league/49.png
49 - The Joy of Water Pokemon
/video/04-the-joy-of-water-pokemon
/static/thumbnails/04-johto-league/50.png
50 - Got Miltank?
/video/04-got-miltank
/static/thumbnails/04-johto-league/51.png
51 - Fight for the Light!
/video/04-fight-for-the-light
/static/thumbnails/04-johto-league/52.png
52 - Machoke, Machoke Man!
/video/04-machoke-machoke-man'''

Master_Quest = '''/static/thumbnails/05-master-quest/1.png
01 - Around the Whirlpool
/video/05-around-the-whirlpool
/static/thumbnails/05-master-quest/2.png
02 - Fly Me to the Moon
/video/05-fly-me-to-the-moon
/static/thumbnails/05-master-quest/3.png
03 - Takin' It on the Chinchou
/video/05-takin-it-on-the-chinchou
/static/thumbnails/05-master-quest/4.png
04 - A Corsola Caper!
/video/05-a-corsola-caper
/static/thumbnails/05-master-quest/5.png
05 - Mantine Overboard!
/video/05-mantine-overboard
/static/thumbnails/05-master-quest/6.png
06 - Octillery The Outcast
/video/05-octillery-the-outcast
/static/thumbnails/05-master-quest/7.png
07 - Dueling Heroes
/video/05-dueling-heroes
/static/thumbnails/05-master-quest/8.png
08 - The Perfect Match!
/video/05-the-perfect-match
/static/thumbnails/05-master-quest/9.png
09 - Plant It Now... Diglett Later
/video/05-plant-it-now-diglett-later
/static/thumbnails/05-master-quest/10.png
10 - Hi Ho Silver... Away!
/video/05-hi-ho-silver-away
/static/thumbnails/05-master-quest/11.png
11 - The Mystery is History
/video/05-the-mystery-is-history
/static/thumbnails/05-master-quest/12.png
12 - A Parent Trapped!
/video/05-a-parent-trapped
/static/thumbnails/05-master-quest/13.png
13 - A Promise is a Promise
/video/05-a-promise-is-a-promise
/static/thumbnails/05-master-quest/14.png
14 - Throwing in the Noctowl
/video/05-throwing-in-the-noctowl
/static/thumbnails/05-master-quest/15.png
15 - Nerves of Steelix!
/video/05-nerves-of-steelix
/static/thumbnails/05-master-quest/16.png
16 - Bulbasaur... the Ambassador!
/video/05-bulbasaur-the-ambassador
/static/thumbnails/05-master-quest/17.png
17 - Espeon, Not Included
/video/05-espeon-not-included
/static/thumbnails/05-master-quest/18.png
18 - For Ho-Oh the Bells Toll!
/video/05-for-ho-oh-the-bells-toll
/static/thumbnails/05-master-quest/19.png
19 - Extreme Pokemon!
/video/05-extreme-pokemon
/static/thumbnails/05-master-quest/20.png
20 - An EGG-sighting Adventure!
/video/05-an-egg-sighting-adventure
/static/thumbnails/05-master-quest/21.png
21 - Hatching a Plan
/video/05-hatching-a-plan
/static/thumbnails/05-master-quest/22.png
22 - Dues and Don'ts
/video/05-dues-and-donts
/static/thumbnails/05-master-quest/23.png
23 - Just Waiting On a Friend
/video/05-just-waiting-on-a-friend
/static/thumbnails/05-master-quest/24.png
24 - A Tyrogue Full of Trouble
/video/05-a-tyrogue-full-of-trouble
/static/thumbnails/05-master-quest/25.png
25 - Xatu the Future
/video/05-xatu-the-future
/static/thumbnails/05-master-quest/26.png
26 - Talkin' 'Bout an Evolution
/video/05-talkin-bout-an-evolution
/static/thumbnails/05-master-quest/27.png
27 - Rage of Innocence
/video/05-rage-of-innocence
/static/thumbnails/05-master-quest/28.png
28 - As Cold as Pryce
/video/05-as-cold-as-pryce
/static/thumbnails/05-master-quest/29.png
29 - Nice Pryce, Baby!
/video/05-nice-pryce-baby
/static/thumbnails/05-master-quest/30.png
30 - Whichever Way the Wind Blows
/video/05-whichever-way-the-wind-blows
/static/thumbnails/05-master-quest/31.png
31 - Some Like it Hot
/video/05-some-like-it-hot
/static/thumbnails/05-master-quest/32.png
32 - Hocus Pokemon
/video/05-hocus-pokemon
/static/thumbnails/05-master-quest/33.png
33 - As Clear as Crystal
/video/05-as-clear-as-crystal
/static/thumbnails/05-master-quest/34.png
34 - Same Old Song and Dance
/video/05-same-old-song-and-dance
/static/thumbnails/05-master-quest/35.png
35 - Enlighten Up!
/video/05-enlighten-up
/static/thumbnails/05-master-quest/36.png
36 - Will the Real Oak Please Stand Up?
/video/05-will-the-real-oak-please-stand-up
/static/thumbnails/05-master-quest/37.png
37 - Wish Upon a Star Shape
/video/05-wish-upon-a-star-shape
/static/thumbnails/05-master-quest/38.png
38 - Outrageous Fortunes
/video/05-outrageous-fortunes
/static/thumbnails/05-master-quest/39.png
39 - One Trick Phony!
/video/05-one-trick-phony
/static/thumbnails/05-master-quest/40.png
40 - I Politoed Ya So!
/video/05-i-politoed-ya-so
/static/thumbnails/05-master-quest/41.png
41 - The Ice Cave
/video/05-the-ice-cave
/static/thumbnails/05-master-quest/42.png
42 - Beauty is Skin Deep
/video/05-beauty-is-skin-deep
/static/thumbnails/05-master-quest/43.png
43 - Fangs for Nothin'
/video/05-fangs-for-nothin
/static/thumbnails/05-master-quest/44.png
44 - Great Bowls of Fire!
/video/05-great-bowls-of-fire
/static/thumbnails/05-master-quest/45.png
45 - Better Eight Than Never
/video/05-better-eight-than-never
/static/thumbnails/05-master-quest/46.png
46 - Why? Wynaut!
/video/05-why-wynaut
/static/thumbnails/05-master-quest/47.png
47 - Just Add Water
/video/05-just-add-water
/static/thumbnails/05-master-quest/48.png
48 - Lapras of Luxury
/video/05-lapras-of-luxury
/static/thumbnails/05-master-quest/49.png
49 - Hatch Me If You Can
/video/05-hatch-me-if-you-can
/static/thumbnails/05-master-quest/50.png
50 - Entei at Your Own Risk
/video/05-entei-at-your-own-risk
/static/thumbnails/05-master-quest/51.png
51 - A Crowning Achievement
/video/05-a-crowning-achievement
/static/thumbnails/05-master-quest/52.png
52 - Here's Lookin' at You, Elekid
/video/05-heres-lookin-at-you-elekid
/static/thumbnails/05-master-quest/53.png
53 - You're a Star, Larvitar!
/video/05-youre-a-star-larvitar
/static/thumbnails/05-master-quest/54.png
54 - Address Unown!
/video/05-address-unown
/static/thumbnails/05-master-quest/55.png
55 - Mother of All Battles
/video/05-mother-of-all-battles
/static/thumbnails/05-master-quest/56.png
56 - Pop Goes The Sneasel
/video/05-pop-goes-the-sneasel
/static/thumbnails/05-master-quest/57.png
57 - A Claim to Flame!
/video/05-a-claim-to-flame
/static/thumbnails/05-master-quest/58.png
58 - Love, Pokemon Style
/video/05-love-pokemon-style
/static/thumbnails/05-master-quest/59.png
59 - Tie One On!
/video/05-tie-one-on
/static/thumbnails/05-master-quest/60.png
60 - The Ties That Bind
/video/05-the-ties-that-bind
/static/thumbnails/05-master-quest/61.png
61 - Can't Beat the Heat!
/video/05-cant-beat-the-heat
/static/thumbnails/05-master-quest/62.png
62 - Playing with Fire!
/video/05-playing-with-fire
/static/thumbnails/05-master-quest/63.png
63 - Johto Photo Finish
/video/05-johto-photo-finish
/static/thumbnails/05-master-quest/64.png
64 - Gotta Catch Ya Later!
/video/05-gotta-catch-ya-later
/static/thumbnails/05-master-quest/65.png
65 - Hoenn Alone!
/video/05-hoenn-alone'''

Advanced = '''/static/thumbnails/06-advanced/1.png
01 - Get the Show on the Road!
/video/06-get-the-show-on-the-road
/static/thumbnails/06-advanced/2.png
02 - A Ruin with a View
/video/06-a-ruin-with-a-view
/static/thumbnails/06-advanced/3.png
03 - There's no Place Like Hoenn
/video/06-theres-no-place-like-hoenn
/static/thumbnails/06-advanced/4.png
04 - You Never Can Taillow
/video/06-you-never-can-taillow
/static/thumbnails/06-advanced/5.png
05 - In the Knicker of Time!
/video/06-in-the-knicker-of-time
/static/thumbnails/06-advanced/6.png
06 - A Poached Ego!
/video/06-a-poached-ego
/static/thumbnails/06-advanced/7.png
07 - Tree's a Crowd
/video/06-trees-a-crowd
/static/thumbnails/06-advanced/8.png
08 - A Tail with a Twist
/video/06-a-tail-with-a-twist
/static/thumbnails/06-advanced/9.png
09 - Taming of the Shroomish
/video/06-taming-of-the-shroomish
/static/thumbnails/06-advanced/10.png
10 - You Said a Mouthful!
/video/06-you-said-a-mouthful
/static/thumbnails/06-advanced/11.png
11 - A Bite To Remember
/video/06-a-bite-to-remember
/static/thumbnails/06-advanced/12.png
12 - The Lotad Lowdown
/video/06-the-lotad-lowdown
/static/thumbnails/06-advanced/13.png
13 - All Things Bright and Beautifly!
/video/06-all-things-bright-and-beautifly
/static/thumbnails/06-advanced/14.png
14 - All in a Day's Wurmple
/video/06-all-in-a-days-wurmple
/static/thumbnails/06-advanced/15.png
15 - Gonna Rule The School!
/video/06-gonna-rule-the-school
/static/thumbnails/06-advanced/16.png
16 - The Winner by a Nosepass
/video/06-the-winner-by-a-nosepass
/static/thumbnails/06-advanced/17.png
17 - Stairway to Devon
/video/06-stairway-to-devon
/static/thumbnails/06-advanced/18.png
18 - On a Wingull and a Prayer!
/video/06-on-a-wingull-and-a-prayer
/static/thumbnails/06-advanced/19.png
19 - Sharpedo Attack!
/video/06-sharpedo-attack
/static/thumbnails/06-advanced/20.png
20 - Brave the Wave
/video/06-brave-the-wave
/static/thumbnails/06-advanced/21.png
21 - Which Wurmple's Which?
/video/06-which-wurmples-which
/static/thumbnails/06-advanced/22.png
22 - A Hole Lotta Trouble
/video/06-a-hole-lotta-trouble
/static/thumbnails/06-advanced/23.png
23 - Gone Corphishin'
/video/06-gone-corphishin
/static/thumbnails/06-advanced/24.png
24 - A Corphish Out of Water
/video/06-a-corphish-out-of-water
/static/thumbnails/06-advanced/25.png
25 - A Mudkip Mission
/video/06-a-mudkip-mission
/static/thumbnails/06-advanced/26.png
26 - Turning Over a Nuzleaf
/video/06-turning-over-a-nuzleaf
/static/thumbnails/06-advanced/27.png
27 - A Three Team Scheme!
/video/06-a-three-team-scheme
/static/thumbnails/06-advanced/28.png
28 - Seeing is Believing!
/video/06-seeing-is-believing
/static/thumbnails/06-advanced/29.png
29 - Ready, Willing, and Sableye
/video/06-ready-willing-and-sableye
/static/thumbnails/06-advanced/30.png
30 - A Meditite Fight!
/video/06-a-meditite-fight
/static/thumbnails/06-advanced/31.png
31 - Just One of the Geysers
/video/06-just-one-of-the-geysers
/static/thumbnails/06-advanced/32.png
32 - Abandon Ship!
/video/06-abandon-ship
/static/thumbnails/06-advanced/33.png
33 - Now That's Flower Power!
/video/06-now-thats-flower-power
/static/thumbnails/06-advanced/34.png
34 - Having a Wailord of a Time
/video/06-having-a-wailord-of-a-time
/static/thumbnails/06-advanced/35.png
35 - Win, Lose or Drew!
/video/06-win-lose-or-drew
/static/thumbnails/06-advanced/36.png
36 - The Spheal of Approval
/video/06-the-spheal-of-approval
/static/thumbnails/06-advanced/37.png
37 - Jump for Joy!
/video/06-jump-for-joy
/static/thumbnails/06-advanced/38.png
38 - A Different Kind of Misty!
/video/06-a-different-kind-of-misty
/static/thumbnails/06-advanced/39.png
39 - A Poke-BLOCK Party!
/video/06-a-poke-block-party
/static/thumbnails/06-advanced/40.png
40 - Watt's with Wattson?
/video/06-watts-with-wattson'''

Advanced_Challenge = '''/static/thumbnails/07-advanced-challenge/1.png
01 - What You Seed is What You Get
/video/07-what-you-seed-is-what-you-get
/static/thumbnails/07-advanced-challenge/2.png
02 - Love at First Flight
/video/07-love-at-first-flight
/static/thumbnails/07-advanced-challenge/3.png
03 - Let Bagons Be Bagons
/video/07-let-bagons-be-bagons
/static/thumbnails/07-advanced-challenge/4.png
04 - The Princess and the Togepi
/video/07-the-princess-and-the-togepi
/static/thumbnails/07-advanced-challenge/5.png
05 - A Togepi Mirage!
/video/07-a-togepi-mirage
/static/thumbnails/07-advanced-challenge/6.png
06 - Candid Camerupt!
/video/07-candid-camerupt
/static/thumbnails/07-advanced-challenge/7.png
07 - I Feel Skitty!
/video/07-i-feel-skitty
/static/thumbnails/07-advanced-challenge/8.png
08 - ZigZag Zangoose!
/video/07-zigzag-zangoose
/static/thumbnails/07-advanced-challenge/9.png
09 - Maxxed Out!
/video/07-maxxed-out
/static/thumbnails/07-advanced-challenge/10.png
10 - Pros and Con Artists
/video/07-pros-and-con-artists
/static/thumbnails/07-advanced-challenge/11.png
11 - Come What May!
/video/07-come-what-may
/static/thumbnails/07-advanced-challenge/12.png
12 - Cheer Pressure
/video/07-cheer-pressure
/static/thumbnails/07-advanced-challenge/13.png
13 - Game Winning Assist
/video/07-game-winning-assist
/static/thumbnails/07-advanced-challenge/14.png
14 - Fight for the Meteorite!
/video/07-fight-for-the-meteorite
/static/thumbnails/07-advanced-challenge/15.png
15 - Poetry Commotion!
/video/07-poetry-commotion
/static/thumbnails/07-advanced-challenge/16.png
16 - Going, Going, Yawn
/video/07-going-going-yawn
/static/thumbnails/07-advanced-challenge/17.png
17 - Going for a Spinda
/video/07-going-for-a-spinda
/static/thumbnails/07-advanced-challenge/18.png
18 - All Torkoal, No Play
/video/07-all-torkoal-no-play
/static/thumbnails/07-advanced-challenge/19.png
19 - Manectric Charge
/video/07-manectric-charge
/static/thumbnails/07-advanced-challenge/20.png
20 - Delcatty Got Your Tongue
/video/07-delcatty-got-your-tongue
/static/thumbnails/07-advanced-challenge/21.png
21 - Disaster of Disguise
/video/07-disaster-of-disguise
/static/thumbnails/07-advanced-challenge/22.png
22 - Disguise Da Limit
/video/07-disguise-da-limit
/static/thumbnails/07-advanced-challenge/23.png
23 - Take the Lombre Home
/video/07-take-the-lombre-home
/static/thumbnails/07-advanced-challenge/24.png
24 - True Blue Swablu
/video/07-true-blue-swablu
/static/thumbnails/07-advanced-challenge/25.png
25 - Gulpin it Down
/video/07-gulpin-it-down
/static/thumbnails/07-advanced-challenge/26.png
26 - Exploud and Clear!
/video/07-exploud-and-clear
/static/thumbnails/07-advanced-challenge/27.png
27 - Go Go Ludicolo!
/video/07-go-go-ludicolo
/static/thumbnails/07-advanced-challenge/28.png
28 - A Double Dilemma
/video/07-a-double-dilemma
/static/thumbnails/07-advanced-challenge/29.png
29 - Love, Petalburg Style!
/video/07-love-petalburg-style!
/static/thumbnails/07-advanced-challenge/30.png
30 - Balance of Power
/video/07-balance-of-power
/static/thumbnails/07-advanced-challenge/31.png
31 - A Six Pack Attack!
/video/07-a-six-pack-attack
/static/thumbnails/07-advanced-challenge/32.png
32 - The Bicker the Better
/video/07-the-bicker-the-better
/static/thumbnails/07-advanced-challenge/33.png
33 - Grass Hysteria!
/video/07-grass-hysteria
/static/thumbnails/07-advanced-challenge/34.png
34 - Hokey Poke Balls!
/video/07-hokey-poke-balls
/static/thumbnails/07-advanced-challenge/35.png
35 - Whiscash and Ash
/video/07-whiscash-and-ash
/static/thumbnails/07-advanced-challenge/36.png
36 - Me, Myself and Time
/video/07-me-myself-and-time
/static/thumbnails/07-advanced-challenge/37.png
37 - A Fan with a Plan
/video/07-a-fan-with-a-plan
/static/thumbnails/07-advanced-challenge/38.png
38 - Cruisin' for a Losin'
/video/07-cruisin-for-a-losin
/static/thumbnails/07-advanced-challenge/39.png
39 - Pearls are a Spoink's Best Friend
/video/07-pearls-are-spoinks-best-friend
/static/thumbnails/07-advanced-challenge/40.png
40 - That's Just Swellow
/video/07-thats-just-swellow
/static/thumbnails/07-advanced-challenge/41.png
41 - Take This House and Shuppet
/video/07-take-this-house-and-shuppet
/static/thumbnails/07-advanced-challenge/42.png
42 - A Shroomish Skirmish
/video/07-a-shroomish-skirmish
/static/thumbnails/07-advanced-challenge/43.png
43 - Unfair-Weather Friends
/video/07-unfair-weather-friends
/static/thumbnails/07-advanced-challenge/44.png
44 - Who's Flying Now?
/video/07-whos-flying-now
/static/thumbnails/07-advanced-challenge/45.png
45 - Sky High Gym Battle!
/video/07-sky-high-gym-battle
/static/thumbnails/07-advanced-challenge/46.png
46 - Lights, Camerupt, Action!
/video/07-lights-camerput-action
/static/thumbnails/07-advanced-challenge/47.png
47 - Crazy as a Lunatone
/video/07-crazy-as-a-lunatone
/static/thumbnails/07-advanced-challenge/48.png
48 - The Garden of Eatin'
/video/07-the-garden-of-eatin
/static/thumbnails/07-advanced-challenge/49.png
49 - A Scare to Remember
/video/07-a-scare-to-remember
/static/thumbnails/07-advanced-challenge/50.png
50 - Pokeblock, Stock, and Berry
/video/07-pokeblock-stock-and-berry
/static/thumbnails/07-advanced-challenge/51.png
51 - Lessons in Lilycove
/video/07-lessons-in-lilycove
/static/thumbnails/07-advanced-challenge/52.png
52 - Judgement Day
/video/07-judgement-day'''

Advanced_Battle = '''
/static/thumbnails/08-advanced-battle/1.png
01 - Clamperl of Wisdom
/video/08-clamperl-of-wisdom
/static/thumbnails/08-advanced-battle/2.png
02 - The Relicanth Really Can
/video/08-the-relicanth-really-can
/static/thumbnails/08-advanced-battle/3.png
03 - The Evolutionary War
/video/08-the-evolutionary-war
/static/thumbnails/08-advanced-battle/4.png
04 - Training Wrecks
/video/08-training-wrecks
/static/thumbnails/08-advanced-battle/5.png
05 - Gaining Groudon
/video/08-gaining-groudon
/static/thumbnails/08-advanced-battle/6.png
06 - The Scuffle of Legends
/video/08-the-scuffle-of-legends
/static/thumbnails/08-advanced-battle/7.png
07 - It's Still Rocket Roll to Me
/video/08-its-still-rocket-roll-to-me
/static/thumbnails/08-advanced-battle/8.png
08 - Solid as a Solrock
/video/08-solid-as-a-solrock
/static/thumbnails/08-advanced-battle/9.png
09 - Vanity Affair
/video/08-vanity-affair
/static/thumbnails/08-advanced-battle/10.png
10 - Where's Armaldo?
/video/08-wheres-armaldo
/static/thumbnails/08-advanced-battle/11.png
11 - A Cacturne for the Worse
/video/08-a-cacturne-for-the-worse
/static/thumbnails/08-advanced-battle/12.png
12 - Claydol Big and Tall
/video/08-claydol-big-and-tall
/static/thumbnails/08-advanced-battle/13.png
13 - Once in a Mawile
/video/08-once-in-a-mawile
/static/thumbnails/08-advanced-battle/14.png
14 - Beg, Burrow and Steal
/video/08-beg-burrow-and-steal
/static/thumbnails/08-advanced-battle/15.png
15 - Absol-ute Disaster!
/video/08-absol-ute-disaster
/static/thumbnails/08-advanced-battle/16.png
16 - Let it Snow, Let it Snow, Let it Snorunt
/video/08-let-it-snow-let-it-snow-let-it-snorunt
/static/thumbnails/08-advanced-battle/17.png
17 - Do I Hear a Ralts?
/video/08-do-i-hear-a-ralts
/static/thumbnails/08-advanced-battle/18.png
18 - The Great Eight Fate!
/video/08-the-great-eight-fate
/static/thumbnails/08-advanced-battle/19.png
19 - Eight Ain't Enough
/video/08-eight-aint-enough
/static/thumbnails/08-advanced-battle/20.png
20 - Showdown at Linoone
/video/08-showdown-at-linoone
/static/thumbnails/08-advanced-battle/21.png
21 - Who, What, When, Where, Wynaut?
/video/08-who-what-when-where-wynaut
/static/thumbnails/08-advanced-battle/22.png
22 - Date Expectations
/video/08-date-expectations
/static/thumbnails/08-advanced-battle/23.png
23 - Mean with Envy
/video/08-mean-with-envy
/static/thumbnails/08-advanced-battle/24.png
24 - Pacifidlog Jam
/video/08-pacifidlog-jam
/static/thumbnails/08-advanced-battle/25.png
25 - Berry, Berry Interesting
/video/08-berry-berry-interesting
/static/thumbnails/08-advanced-battle/26.png
26 - Less is Morrison
/video/08-less-is-morrison
/static/thumbnails/08-advanced-battle/27.png
27 - The Ribbon Cup Caper!
/video/08-the-ribbon-cup-caper
/static/thumbnails/08-advanced-battle/28.png
28 - Satoshi and Haruka! Heated Battles in Hoenn!!
/video/08-satoshi-and-haruka-heated-battles-in-hoenn
/static/thumbnails/08-advanced-battle/29.png
29 - Hi Ho Silver Wind!
/video/08-hi-ho-silver-wind
/static/thumbnails/08-advanced-battle/30.png
30 - Deceit and Assist
/video/08-deceit-and-assist
/static/thumbnails/08-advanced-battle/31.png
31 - Rhapsody in Drew
/video/08-rhapsody-in-drew
/static/thumbnails/08-advanced-battle/32.png
32 - Island Time
/video/08-island-time
/static/thumbnails/08-advanced-battle/33.png
33 - Like a Meowth to a Flame
/video/08-like-a-meowth-to-a-flame
/static/thumbnails/08-advanced-battle/34.png
34 - Saved by the Beldum
/video/08-saved-by-the-beldum
/static/thumbnails/08-advanced-battle/35.png
35 - From Brags to Riches
/video/08-from-brags-to-riches
/static/thumbnails/08-advanced-battle/36.png
36 - Shocks and Bonds
/video/08-shocks-and-bonds
/static/thumbnails/08-advanced-battle/37.png
37 - A Judgement Brawl
/video/08-a-judgement-brawl
/static/thumbnails/08-advanced-battle/38.png
38 - Choose It or Lose It!
/video/08-choose-it-or-lose-it
/static/thumbnails/08-advanced-battle/39.png
39 - At the End of the Fray
/video/08-at-the-end-of-the-fray
/static/thumbnails/08-advanced-battle/40.png
40 - The Scheme Team
/video/08-the-scheme-team
/static/thumbnails/08-advanced-battle/41.png
41 - The Right Place and the Right Mime
/video/08-the-right-place-and-the-right-mime
/static/thumbnails/08-advanced-battle/42.png
42 - A Real Cleffa-Hanger
/video/08-a-real-cleffa-hanger
/static/thumbnails/08-advanced-battle/43.png
43 - Numero Uno Articuno
/video/08-numero-uno-articuno
/static/thumbnails/08-advanced-battle/44.png
44 - The Symbol of Life
/video/08-the-symbol-of-life
/static/thumbnails/08-advanced-battle/45.png
45 - Hooked on Onix
/video/08-hooked-on-onix
/static/thumbnails/08-advanced-battle/46.png
46 - Rough, Tough, Jigglypuff
/video/08-rough-tough-jigglypuff
/static/thumbnails/08-advanced-battle/47.png
47 - On Cloud Arcanine
/video/08-on-cloud-arcanine
/static/thumbnails/08-advanced-battle/48.png
48 - Sitting Psyduck
/video/08-sitting-psyduck
/static/thumbnails/08-advanced-battle/49.png
49 - Hail to the Chef!
/video/08-hail-to-the-chef
/static/thumbnails/08-advanced-battle/50.png
50 - Caterpie's Big Dilemma
/video/08-caterpies-big-dilemma
/static/thumbnails/08-advanced-battle/51.png
51 - The Saffron Con
/video/08-the-saffron-con
/static/thumbnails/08-advanced-battle/52.png
52 - A Hurdle for Squirtle
/video/08-a-hurdle-for-squirtle
/static/thumbnails/08-advanced-battle/53.png
53 - Pasta La Vista
/video/08-pasta-la-vista
'''

Battle_Frontier = '''
/static/thumbnails/09-battle-frontier/1.png
01 - Fear Factor Phony
/video/09-fear-factor-phony
/static/thumbnails/09-battle-frontier/2.png
02 - Sweet Baby James
/video/09-sweet-baby-james
/static/thumbnails/09-battle-frontier/3.png
03 - A Chip Off the Old Brock
/video/09-a-chip-off-the-old-brock
/static/thumbnails/09-battle-frontier/4.png
04 - Wheel of Frontier
/video/09-wheel-of-frontier
/static/thumbnails/09-battle-frontier/5.png
05 - May's Egg-cellent Adventure!
/video/09-mays-egg-cellent-adventure
/static/thumbnails/09-battle-frontier/6.png
06 - Weekend Warrior
/video/09-weekend-warrior
/static/thumbnails/09-battle-frontier/7.png
07 - On Olden Pond
/video/09-on-olden-pond
/static/thumbnails/09-battle-frontier/8.png
08 - Tactics Theatrics!!
/video/09-tactics-theatrics
/static/thumbnails/09-battle-frontier/9.png
09 - Reversing the Charges
/video/09-reversing-the-charges
/static/thumbnails/09-battle-frontier/10.png
10 - The Green Guardian
/video/09-the-green-guardian
/static/thumbnails/09-battle-frontier/11.png
11 - From Cradle to Save!
/video/09-from-cradle-to-save
/static/thumbnails/09-battle-frontier/12.png
12 - Time-Warp Heals All Wounds!
/video/09-time-warp-heals-all-wounds
/static/thumbnails/09-battle-frontier/13.png
13 - Queen of the Serpentine!
/video/09-queen-of-the-serpentine
/static/thumbnails/09-battle-frontier/14.png
14 - Off the Unbeaten Path!
/video/09-off-the-unbeaten-path
/static/thumbnails/09-battle-frontier/15.png
15 - Harley Rides Again
/video/09-harley-rides-again
/static/thumbnails/09-battle-frontier/16.png
16 - Odd Pokemon Out
/video/09-odd-pokemon-out
/static/thumbnails/09-battle-frontier/17.png
17 - Spontaneous Combusken
/video/09-spontaneous-combusken
/static/thumbnails/09-battle-frontier/18.png
18 - Cutting the Ties That Bind
/video/09-cutting-the-ties-that-bind
/static/thumbnails/09-battle-frontier/19.png
19 - Kaboom with a View
/video/09-kaboom-with-a-view
/static/thumbnails/09-battle-frontier/20.png
20 - King and Queen for a Day
/video/09-king-and-queen-for-a-day
/static/thumbnails/09-battle-frontier/21.png
21 - Curbing the Crimson Tide
/video/09-curbing-the-crimson-tide
/static/thumbnails/09-battle-frontier/22.png
22 - What I Did for Love
/video/09-what-i-did-for-love
/static/thumbnails/09-battle-frontier/23.png
23 - Three Jynx and a Baby
/video/09-three-jynx-and-a-baby
/static/thumbnails/09-battle-frontier/24.png
24 - Talking a Good Game!
/video/09-talking-a-good-game
/static/thumbnails/09-battle-frontier/25.png
25 - Second Time's the Charm!
/video/09-second-times-the-charm
/static/thumbnails/09-battle-frontier/26.png
26 - Pokemon Ranger! Deoxys Crisis! (Part One)
/video/09-pokemon-ranger-deoxys-crisis-part-one
/static/thumbnails/09-battle-frontier/27.png
27 - Pokemon Ranger! Deoxys Crisis! (Part Two)
/video/09-pokemon-ranger-deoxys-crisis-part-two
/static/thumbnails/09-battle-frontier/28.png
28 - All That Glitters is Not Golden!
/video/09-all-that-glitters-is-not-golden
/static/thumbnails/09-battle-frontier/29.png
29 - New Plot, Odd Lot!
/video/09-new-plot-odd-lot
/static/thumbnails/09-battle-frontier/30.png
30 - Going for Choke!
/video/09-going-for-choke
/static/thumbnails/09-battle-frontier/31.png
31 - The Ole' Berate and Switch
/video/09-the-ole-berate-and-switch
/static/thumbnails/09-battle-frontier/32.png
32 - Grating Spaces
/video/09-grating-spaces
/static/thumbnails/09-battle-frontier/33.png
33 - Battling the Enemy Within
/video/09-battling-the-enemy-within
/static/thumbnails/09-battle-frontier/34.png
34 - Slaking Kong
/video/09-slaking-kong
/static/thumbnails/09-battle-frontier/35.png
35 - May, We Harley Drew'd Ya!
/video/09-may-we-harley-drewd-ya
/static/thumbnails/09-battle-frontier/36.png
36 - Thinning the Hoard!
/video/09-thinning-the-hoard
/static/thumbnails/09-battle-frontier/37.png
37 - Channeling the Battle Zone
/video/09-channeling-the-battle-zone
/static/thumbnails/09-battle-frontier/38.png
38 - Aipom and Circumstance!
/video/09-aipom-and-circumstance
/static/thumbnails/09-battle-frontier/39.png
39 - Strategy Tomorrow A_a'_a_oe Comedy Tonight!
/video/09-strategy-tomorrow--comedy-tonight
/static/thumbnails/09-battle-frontier/40.png
40 - Duels of the Jungle
/video/09-duels-of-the-jungle
/static/thumbnails/09-battle-frontier/41.png
41 - Overjoyed!
/video/09-overjoyed
/static/thumbnails/09-battle-frontier/42.png
42 - The Unbeatable Lightness of Seeing
/video/09-the-unbeatable-lightness-of-seeing
/static/thumbnails/09-battle-frontier/43.png
43 - Pinch Healing!
/video/09-pinch-healing
/static/thumbnails/09-battle-frontier/44.png
44 - Gathering the Gang of Four
/video/09-gathering-the-gang-of-four
/static/thumbnails/09-battle-frontier/45.png
45 - Pace A_a'_a_oe The Final Frontier!
/video/09-pace--the-final-frontier
/static/thumbnails/09-battle-frontier/46.png
46 - Once More with Reeling
/video/09-once-more-with-reeling
/static/thumbnails/09-battle-frontier/47.png
47 - Home is Where the Start Is
/video/09-home-is-where-the-start-is
'''

Diamond_and_Pearl = '''
/static/thumbnails/10-diamond-pearl/1.png
01 - Following a Maiden's Voyage!
/video/10-following-a-maidens-voyage
/static/thumbnails/10-diamond-pearl/2.png
02 - Two Degrees of Separation!
/video/10-two-degrees-of-separation
/static/thumbnails/10-diamond-pearl/3.png
03 - When Pokemon Worlds Collide!
/video/10-when-pokemon-worlds-collide
/static/thumbnails/10-diamond-pearl/4.png
04 - Dawn of a New Era!
/video/10-dawn-of-a-new-era
/static/thumbnails/10-diamond-pearl/5.png
05 - Gettin' Twiggy with It!
/video/10-gettin-twiggy-with-it
/static/thumbnails/10-diamond-pearl/6.png
06 - Different Strokes for Different Blokes!
/video/10-different-strokes-for-different-blokes
/static/thumbnails/10-diamond-pearl/7.png
07 - Like it or Lup It!
/video/10-like-it-or-lup-it
/static/thumbnails/10-diamond-pearl/8.png
08 - Gymbaliar!
/video/10-gymbaliar
/static/thumbnails/10-diamond-pearl/9.png
09 - Setting the World on its Buneary!
/video/10-setting-the-world-on-its-buneary
/static/thumbnails/10-diamond-pearl/10.png
10 - Not on My Watch Ya Don't!
/video/10-not-on-my-watch-ya-dont
/static/thumbnails/10-diamond-pearl/11.png
11 - Mounting a Coordinator Assault!
/video/10-mounting-a-coordinator-assault
/static/thumbnails/10-diamond-pearl/12.png
12 - Arrival of a Rival!
/video/10-arrival-of-a-rival
/static/thumbnails/10-diamond-pearl/13.png
13 - A Staravia Is Born!
/video/10-a-staravia-is-born
/static/thumbnails/10-diamond-pearl/14.png
14 - Leave it to Brocko!
/video/10-leave-it-to-brocko
/static/thumbnails/10-diamond-pearl/15.png
15 - Shapes of Things to Come!
/video/10-shapes-of-things-to-come
/static/thumbnails/10-diamond-pearl/16.png
16 - A Gruff Act to Follow!
/video/10-a-gruff-act-to-follow
/static/thumbnails/10-diamond-pearl/17.png
17 - Wild in the Streets!
/video/10-wild-in-the-streets
/static/thumbnails/10-diamond-pearl/18.png
18 - O'er the Rampardos We Watched!
/video/10-oer-the-rampardos-we-watched
/static/thumbnails/10-diamond-pearl/19.png
19 - Twice Smitten, Once Shy!
/video/10-twice-smitten-once-shy
/static/thumbnails/10-diamond-pearl/20.png
20 - Mutiny in the Bounty!
/video/10-mutiny-in-the-bounty
/static/thumbnails/10-diamond-pearl/21.png
21 - Ya See We Want an Evolution!
/video/10-ya-see-we-want-an-evolution
/static/thumbnails/10-diamond-pearl/22.png
22 - Borrowing on Bad Faith!
/video/10-borrowing-on-bad-faith
/static/thumbnails/10-diamond-pearl/23.png
23 - Faced with Steelix Determination!
/video/10-faced-with-steelix-determination
/static/thumbnails/10-diamond-pearl/24.png
24 - Cooking Up a Sweet Story!
/video/10-cooking-up-a-sweet-story
/static/thumbnails/10-diamond-pearl/25.png
25 - Oh Do You Know the Poffin Plan!
/video/10-oh-do-you-know-the-poffin-plan
/static/thumbnails/10-diamond-pearl/26.png
26 - Getting the Pre-Contest Titters!
/video/10-getting-the-pre-contest-titters
/static/thumbnails/10-diamond-pearl/27.png
27 - Settling a Not-So-Old Score!
/video/10-settling-a-not-so-old-score
/static/thumbnails/10-diamond-pearl/28.png
28 - Drifloon on the Wind!
/video/10-drifloon-on-the-wind
/static/thumbnails/10-diamond-pearl/29.png
29 - The Champ Twins!
/video/10-the-champ-twins
/static/thumbnails/10-diamond-pearl/30.png
30 - Some Enchanted Sweetening!
/video/10-some-enchanted-sweetening
/static/thumbnails/10-diamond-pearl/31.png
31 - The Grass-Type is Always Greener!
/video/10-the-grass-type-is-always-greener
/static/thumbnails/10-diamond-pearl/32.png
32 - An Angry Combeenation!
/video/10-an-angry-combeenation
/static/thumbnails/10-diamond-pearl/33.png
33 - All Dressed Up with Somewhere to Go!
/video/10-all-dressed-up-with-somewhere-to-go
/static/thumbnails/10-diamond-pearl/34.png
34 - Buizel Your Way Out of This!
/video/10-buizel-your-way-out-of-this
/static/thumbnails/10-diamond-pearl/35.png
35 - An Elite Meet and Greet
/video/10-an-elite-meet-and-greet
/static/thumbnails/10-diamond-pearl/36.png
36 - A Secret Sphere of Influence
/video/10-a-secret-sphere-of-influence
/static/thumbnails/10-diamond-pearl/37.png
37 - The Grass Menagerie
/video/10-the-grass-menagerie
/static/thumbnails/10-diamond-pearl/38.png
38 - One Big Happiny Family
/video/10-one-big-happiny-family
/static/thumbnails/10-diamond-pearl/39.png
39 - Steamboat Willies
/video/10-steamboat-willies
/static/thumbnails/10-diamond-pearl/40.png
40 - Top-Down Training
/video/10-top-down-training
/static/thumbnails/10-diamond-pearl/41.png
41 - A Stand Up Sit Down
/video/10-a-stand-up-sit-down
/static/thumbnails/10-diamond-pearl/42.png
42 - The Electrike Company
/video/10-the-electrike-company
/static/thumbnails/10-diamond-pearl/43.png
43 - Malice in Wonderland!
/video/10-malice-in-wonderland
/static/thumbnails/10-diamond-pearl/44.png
44 - Mass Hip-po-sis!
/video/10-mass-hip-po-sis
/static/thumbnails/10-diamond-pearl/45.png
45 - Ill-Will Hunting
/video/10-ill-will-hunting
/static/thumbnails/10-diamond-pearl/46.png
46 - A Maze-ing Race!
/video/10-a-maze-ing-race
/static/thumbnails/10-diamond-pearl/47.png
47 - Sandshrew's Locker!
/video/10-sandshrews-locker
/static/thumbnails/10-diamond-pearl/48.png
48 - Dawn's Early Night!
/video/10-dawns-early-night
/static/thumbnails/10-diamond-pearl/49.png
49 - Tag! We're It...!
/video/10-tag-were-it
/static/thumbnails/10-diamond-pearl/50.png
50 - Glory Blaze!
/video/10-glory-blaze
/static/thumbnails/10-diamond-pearl/51.png
51 - Smells Like Team Spirit!
/video/10-smells-like-team-spirit
'''

DP_Battle_Dimension = '''
/static/thumbnails/11-dp-battle-dimension/1.png
01 - Tears for Fears!
/video/11-tears-for-fears
/static/thumbnails/11-dp-battle-dimension/2.png
02 - Once There Were Greenfields
/video/11-once-there-were-greenfields
/static/thumbnails/11-dp-battle-dimension/3.png
03 - Throwing the Track Switch
/video/11-throwing-the-track-switch
/static/thumbnails/11-dp-battle-dimension/4.png
04 - The Keystone Pops!
/video/11-the-keystone-pops
/static/thumbnails/11-dp-battle-dimension/5.png
05 - Bibarel Gnaws Best!
/video/11-bibarel-gnaws-best
/static/thumbnails/11-dp-battle-dimension/6.png
06 - Nosing 'Round the Mountain
/video/11-nosing-round-the-mountain
/static/thumbnails/11-dp-battle-dimension/7.png
07 - Luxray Vision
/video/11-luxray-vision
/static/thumbnails/11-dp-battle-dimension/8.png
08 - Journey to the Unown
/video/11-journey-to-the-unown
/static/thumbnails/11-dp-battle-dimension/9.png
09 - Team Shocker
/video/11-team-shocker
/static/thumbnails/11-dp-battle-dimension/10.png
10 - Tanks for the Memories
/video/11-tanks-for-the-memories
/static/thumbnails/11-dp-battle-dimension/11.png
11 - Hot Springing a Leak
/video/11-hot-springing-a-leak
/static/thumbnails/11-dp-battle-dimension/12.png
12 - Riding the Winds of Change
/video/11-riding-the-winds-of-change
/static/thumbnails/11-dp-battle-dimension/13.png
13 - Sleight of Sand
/video/11-sleight-of-sand
/static/thumbnails/11-dp-battle-dimension/14.png
14 - Lost Leader Strategy
/video/11-lost-leader-strategy
/static/thumbnails/11-dp-battle-dimension/15.png
15 - Crossing the Battle Line!
/video/11-crossing-the-battle-line
/static/thumbnails/11-dp-battle-dimension/16.png
16 - A Triple Fighting Chance!
/video/11-a-triple-fighting-chance
/static/thumbnails/11-dp-battle-dimension/17.png
17 - Enter Galactic!
/video/11-enter-galactic
/static/thumbnails/11-dp-battle-dimension/18.png
18 - The Bells Are Singing!
/video/11-the-bells-are-singing
/static/thumbnails/11-dp-battle-dimension/19.png
19 - Pokemon Ranger and the Kidnapped Riolu! Part One
/video/11-pokemon-ranger-and-the-kidnapped-riolu-part-one
/static/thumbnails/11-dp-battle-dimension/20.png
20 - Pokemon Ranger and the Kidnapped Riolu! Part Two
/video/11-pokemon-ranger-and-the-kidnapped-riolu-part-two
/static/thumbnails/11-dp-battle-dimension/21.png
21 - Crossing Paths
/video/11-crossing-paths
/static/thumbnails/11-dp-battle-dimension/22.png
22 - Pika and Goliath
/video/11-pika-and-goliath
/static/thumbnails/11-dp-battle-dimension/23.png
23 - Our Cup Runneth Over!
/video/11-our-cup-runneth-over
/static/thumbnails/11-dp-battle-dimension/24.png
24 - A Full Course Tag Battle!
/video/11-a-full-course-tag-battle
/static/thumbnails/11-dp-battle-dimension/25.png
25 - Staging a Heroes' Welcome!
/video/11-staging-a-heroes-welcome
/static/thumbnails/11-dp-battle-dimension/26.png
26 - Pruning a Passel of Pals!
/video/11-pruning-a-passel-of-pals
/static/thumbnails/11-dp-battle-dimension/27.png
27 - Strategy with a Smile!
/video/11-strategy-with-a-smile
/static/thumbnails/11-dp-battle-dimension/28.png
28 - The Thief That Keeps on Thieving!
/video/11-the-thief-that-keeps-on-thieving
/static/thumbnails/11-dp-battle-dimension/29.png
29 - Chim-Charred!
/video/11-chim-charred
/static/thumbnails/11-dp-battle-dimension/30.png
30 - Cream of the Croagunk Crop!
/video/11-cream-of-the-croagunk-crop
/static/thumbnails/11-dp-battle-dimension/31.png
31 - A Crasher Course in Power!
/video/11-a-crasher-course-in-power
/static/thumbnails/11-dp-battle-dimension/32.png
32 - Hungry for the Good Life!
/video/11-hungry-for-the-good-life
/static/thumbnails/11-dp-battle-dimension/33.png
33 - Fighting Fear with Fear!
/video/11-fighting-fear-with-fear
/static/thumbnails/11-dp-battle-dimension/34.png
34 - Arriving in Style!
/video/11-arriving-in-style
/static/thumbnails/11-dp-battle-dimension/35.png
35 - The Psyduck Stops Here!
/video/11-the-psyduck-stops-here
/static/thumbnails/11-dp-battle-dimension/36.png
36 - Camping it Up!
/video/11-camping-it-up
/static/thumbnails/11-dp-battle-dimension/37.png
37 - Up Close and Personable!
/video/11-up-close-and-personable
/static/thumbnails/11-dp-battle-dimension/38.png
38 - Ghoul Daze!
/video/11-ghoul-daze
/static/thumbnails/11-dp-battle-dimension/39.png
39 - One Team, Two Team, Red Team, Blue Team!
/video/11-one-team-two-team-red-team-blue-team
/static/thumbnails/11-dp-battle-dimension/40.png
40 - A Lean Mean Team Rocket Machine!
/video/11-a-lean-mean-team-rocket-machine
/static/thumbnails/11-dp-battle-dimension/41.png
41 - Playing the Leveling Field!
/video/11-playing-the-leveling-field
/static/thumbnails/11-dp-battle-dimension/42.png
42 - Doc Brock!
/video/11-doc-brock
/static/thumbnails/11-dp-battle-dimension/43.png
43 - Battling the Generation Gap!
/video/11-battling-the-generation-gap
/static/thumbnails/11-dp-battle-dimension/44.png
44 - Losing Its Lustrous!
/video/11-losing-its-lustrous
/static/thumbnails/11-dp-battle-dimension/45.png
45 - Double Team Turnover!
/video/11-double-team-turnover
/static/thumbnails/11-dp-battle-dimension/46.png
46 - If the Scarf Fits, Wear It!
/video/11-if-the-scarf-fits-wear-it
/static/thumbnails/11-dp-battle-dimension/47.png
47 - A Trainer and Child Reunion!
/video/11-a-trainer-and-child-reunion
/static/thumbnails/11-dp-battle-dimension/48.png
48 - Aiding the Enemy!
/video/11-aiding-the-enemy
/static/thumbnails/11-dp-battle-dimension/49.png
49 - Barry's Busting Out All Over!
/video/11-barrys-busting-out-all-over
/static/thumbnails/11-dp-battle-dimension/50.png
50 - Shield with a Twist!
/video/11-shield-with-a-twist
/static/thumbnails/11-dp-battle-dimension/51.png
51 - Jumping Rocket Ship!
/video/11-jumping-rocket-ship
/static/thumbnails/11-dp-battle-dimension/52.png
52 - Sleepless in Pre-Battle
/video/11-sleepless-in-pre-battle
'''

DP_Galactic_Battles = '''
/static/thumbnails/12-dp-galactic-battles/1.png
01 - Get Your Rotom Running!
/video/12-get-your-rotom-running
/static/thumbnails/12-dp-galactic-battles/2.png
02 - A Breed Stampede!
/video/12-a-breed-stampede
/static/thumbnails/12-dp-galactic-battles/3.png
03 - Ancient Family Matters!
/video/12-ancient-family-matters
/static/thumbnails/12-dp-galactic-battles/4.png
04 - Dealing with Defensive Types!
/video/12-dealing-with-defensive-types
/static/thumbnails/12-dp-galactic-battles/5.png
05 - Leading a Stray!
/video/12-leading-a-stray
/static/thumbnails/12-dp-galactic-battles/6.png
06 - Steeling Peace of Mind!
/video/12-steeling-peace-of-mind
/static/thumbnails/12-dp-galactic-battles/7.png
07 - Saving the World from Ruins!
/video/12-saving-the-world-from-ruins
/static/thumbnails/12-dp-galactic-battles/8.png
08 - Cheers on Castaways Isle!
/video/12-cheers-on-castaways-isle
/static/thumbnails/12-dp-galactic-battles/9.png
09 - Hold the Phione!
/video/12-hold-the-phione
/static/thumbnails/12-dp-galactic-battles/10.png
10 - Another One Gabites the Dust!
/video/12-another-one-gabites-the-dust
/static/thumbnails/12-dp-galactic-battles/11.png
11 - Stealing the Conversation!
/video/12-stealing-the-conversation
/static/thumbnails/12-dp-galactic-battles/12.png
12 - The Drifting Snorunt!
/video/12-the-drifting-snorunt
/static/thumbnails/12-dp-galactic-battles/13.png
13 - Noodles! Roamin' Off!
/video/12-noodles-roamin-off
/static/thumbnails/12-dp-galactic-battles/14.png
14 - Pursuing a Lofty Goal!
/video/12-pursuing-a-lofty-goal
/static/thumbnails/12-dp-galactic-battles/15.png
15 - Trials and Adulations!
/video/12-trials-and-adulations
/static/thumbnails/12-dp-galactic-battles/16.png
16 - The Mysterious Creatures, Pocket Monsters!
/video/12-the-mysterious-creatures-pocket-monsters
/static/thumbnails/12-dp-galactic-battles/17.png
17 - The Lonely Snover!
/video/12-the-lonely-snover
/static/thumbnails/12-dp-galactic-battles/18.png
18 - Stopped in the Name of Love!
/video/12-stopped-in-the-name-of-love
/static/thumbnails/12-dp-galactic-battles/19.png
19 - Old Rivals, New Tricks!
/video/12-old-rivals-new-tricks
/static/thumbnails/12-dp-galactic-battles/20.png
20 - To Thine Own Pokemon Be True!
/video/12-to-thine-own-pokemon-be-true
/static/thumbnails/12-dp-galactic-battles/21.png
21 - Battling a Cute Drama!
/video/12-battling-a-cute-drama
/static/thumbnails/12-dp-galactic-battles/22.png
22 - Classroom Training!
/video/12-classroom-training
/static/thumbnails/12-dp-galactic-battles/23.png
23 - Sliding Into Seventh!
/video/12-sliding-into-seventh
/static/thumbnails/12-dp-galactic-battles/24.png
24 - A Pyramiding Rage!
/video/12-a-pyramiding-rage
/static/thumbnails/12-dp-galactic-battles/25.png
25 - Pillars of Friendship!
/video/12-pillars-of-friendship
/static/thumbnails/12-dp-galactic-battles/26.png
26 - Frozen on Their Tracks!
/video/12-frozen-on-their-tracks
/static/thumbnails/12-dp-galactic-battles/27.png
27 - Pedal to the Mettle!
/video/12-pedal-to-the-mettle
/static/thumbnails/12-dp-galactic-battles/28.png
28 - Evolving Strategies!
/video/12-evolving-strategies
/static/thumbnails/12-dp-galactic-battles/29.png
29 - Uncrushing Defeat!
/video/12-uncrushing-defeat
/static/thumbnails/12-dp-galactic-battles/30.png
30 - Promoting Healthy Tangrowth!
/video/12-promoting-healthy-tangrowth
/static/thumbnails/12-dp-galactic-battles/31.png
31 - Beating the Bustle and Hustle!
/video/12-beating-the-bustle-and-hustle
/static/thumbnails/12-dp-galactic-battles/32.png
32 - Gateway to Ruin!
/video/12-gateway-to-ruin
/static/thumbnails/12-dp-galactic-battles/33.png
33 - Three Sides to Every Story
/video/12-three-sides-to-every-story
/static/thumbnails/12-dp-galactic-battles/34.png
34 - Strategy Begins at Home!
/video/12-strategy-begins-at-home
/static/thumbnails/12-dp-galactic-battles/35.png
35 - A Faux Oak Finish!
/video/12-a-faux-oak-finish
/static/thumbnails/12-dp-galactic-battles/36.png
36 - Historical Mystery Tour
/video/12-historical-mystery-tour
/static/thumbnails/12-dp-galactic-battles/37.png
37 - Challenging a Towering Figure
/video/12-challenging-a-towering-figure
/static/thumbnails/12-dp-galactic-battles/38.png
38 - Where No Togepi Has Gone Before!
/video/12-where-no-togepi-has-gone-before
/static/thumbnails/12-dp-galactic-battles/39.png
39 - An Egg Scramble!
/video/12-an-egg-scramble
/static/thumbnails/12-dp-galactic-battles/40.png
40 - Gone With the Windworks!
/video/12-gone-with-the-windworks
/static/thumbnails/12-dp-galactic-battles/41.png
41 - A Rivalry to Gible On!
/video/12-a-rivalry-to-gible-on
/static/thumbnails/12-dp-galactic-battles/42.png
42 - Dressed for Jess Success!
/video/12-dressed-for-jess-success
/static/thumbnails/12-dp-galactic-battles/43.png
43 - Bagged Then Tagged!
/video/12-bagged-then-tagged
/static/thumbnails/12-dp-galactic-battles/44.png
44 - Try for the Family Stone!
/video/12-try-for-the-family-stone
/static/thumbnails/12-dp-galactic-battles/45.png
45 - Sticking with Who You Know!
/video/12-sticking-with-who-you-know
/static/thumbnails/12-dp-galactic-battles/46.png
46 - Unlocking the Red Chain of Events!
/video/12-unlocking-the-red-chain-of-events
/static/thumbnails/12-dp-galactic-battles/47.png
47 - The Needs of the Three!
/video/12-the-needs-of-the-three
/static/thumbnails/12-dp-galactic-battles/48.png
48 - The Battle Finale of Legend!
/video/12-the-battle-finale-of-legend
/static/thumbnails/12-dp-galactic-battles/49.png
49 - The Treasure Is All Mine!
/video/12-the-treasure-is-all-mine
/static/thumbnails/12-dp-galactic-battles/50.png
50 - Mastering Current Events!
/video/12-mastering-current-events
/static/thumbnails/12-dp-galactic-battles/51.png
51 - Double-Time Battle Training!
/video/12-double-time-battle-training
/static/thumbnails/12-dp-galactic-battles/52.png
52 - A Meteoric Rise to Excellence!
/video/12-a-meteoric-rise-to-excellence
/static/thumbnails/12-dp-galactic-battles/53.png
53 - Gotta Get a Gible!
/video/12-gotta-get-a-gible
'''

DP_Sinnoh_League_Victors = '''
/static/thumbnails/13-dp-sinnoh-league/1.png
01 - Regaining the Home Advantage!
/video/13-regaining-the-home-advantage
/static/thumbnails/13-dp-sinnoh-league/2.png
02 - Short and to the Punch!
/video/13-short-and-to-the-punch
/static/thumbnails/13-dp-sinnoh-league/3.png
03 - A Marathon Rivalry!
/video/13-a-marathon-rivalry
/static/thumbnails/13-dp-sinnoh-league/4.png
04 - Yes, in Dee Dee It's Dawn!
/video/13-yes-in-dee-dee-its-dawn
/static/thumbnails/13-dp-sinnoh-league/5.png
05 - Playing the Performance Encore!
/video/13-playing-the-performance-encore
/static/thumbnails/13-dp-sinnoh-league/6.png
06 - Fighting Ire with Fire!
/video/13-fighting-ire-with-fire
/static/thumbnails/13-dp-sinnoh-league/7.png
07 - Piplup, Up and Away!
/video/13-piplup-up-and-away
/static/thumbnails/13-dp-sinnoh-league/8.png
08 - Flint Sparks the Fire!
/video/13-flint-sparks-the-fire
/static/thumbnails/13-dp-sinnoh-league/9.png
09 - The Fleeing Tower of Sunyshore!
/video/13-the-fleeing-tower-of-sunyshore
/static/thumbnails/13-dp-sinnoh-league/10.png
10 - Teaching the Student Teacher
/video/13-teaching-the-student-teacher
/static/thumbnails/13-dp-sinnoh-league/11.png
11 - Keeping in Top Forme!
/video/13-keeping-in-top-forme
/static/thumbnails/13-dp-sinnoh-league/12.png
12 - Pokemon Ranger: Heatran Rescue!
/video/13-pokemon-ranger-heatran-rescue
/static/thumbnails/13-dp-sinnoh-league/13.png
13 - An Elite Coverup!
/video/13-an-elite-coverup
/static/thumbnails/13-dp-sinnoh-league/14.png
14 - Dawn of a Royal Day!
/video/13-dawn-of-a-royal-day
/static/thumbnails/13-dp-sinnoh-league/15.png
15 - With the Easiest of Grace!
/video/13-with-the-easiest-of-grace
/static/thumbnails/13-dp-sinnoh-league/16.png
16 - Dealing with a Fierce Double Ditto Drama!
/video/13-dealing-with-a-fierce-double-ditto-drama
/static/thumbnails/13-dp-sinnoh-league/17.png
17 - Last Call, First Round!
/video/13-last-call-first-round
/static/thumbnails/13-dp-sinnoh-league/18.png
18 - Opposites Interact!
/video/13-opposites-interact
/static/thumbnails/13-dp-sinnoh-league/19.png
19 - Coming Full Festival Circle!
/video/13-coming-full-festival-circle
/static/thumbnails/13-dp-sinnoh-league/20.png
20 - A Grand Fight for Winning!
/video/13-a-grand-fight-for-winning
/static/thumbnails/13-dp-sinnoh-league/21.png
21 - For the Love of Meowth!
/video/13-for-the-love-of-meowth
/static/thumbnails/13-dp-sinnoh-league/22.png
22 - The Eighth Wonder of the Sinnoh World!
/video/13-the-eighth-wonder-of-the-sinnoh-world
/static/thumbnails/13-dp-sinnoh-league/23.png
23 - Four Roads Diverged in a Pokemon Port!
/video/13-four-roads-diverged-in-a-pokemon-port
/static/thumbnails/13-dp-sinnoh-league/24.png
24 - Bucking the Treasure Trend!
/video/13-bucking-the-treasure-trend
/static/thumbnails/13-dp-sinnoh-league/25.png
25 - An Old Family Blend!
/video/13-an-old-family-blend
/static/thumbnails/13-dp-sinnoh-league/26.png
26 - League Unleashed!
/video/13-league-unleashed
/static/thumbnails/13-dp-sinnoh-league/27.png
27 - Casting a Paul on Barry!
/video/13-casting-a-paul-on-barry
/static/thumbnails/13-dp-sinnoh-league/28.png
28 - Working on a Right Move!!
/video/13-working-on-a-right-move
/static/thumbnails/13-dp-sinnoh-league/29.png
29 - Familiarity Breeds Strategy!
/video/13-familiarity-breeds-strategy
/static/thumbnails/13-dp-sinnoh-league/30.png
30 - A Real Rival Rouser!
/video/13-a-real-rival-rouser
/static/thumbnails/13-dp-sinnoh-league/31.png
31 - Battling a Thaw in Relations!
/video/13-battling-a-thaw-in-relations
/static/thumbnails/13-dp-sinnoh-league/32.png
32 - The Semi-Final Frontier
/video/13-the-semi-final-frontier
/static/thumbnails/13-dp-sinnoh-league/33.png
33 - The Brockster Is In!
/video/13-the-brockster-is-in
/static/thumbnails/13-dp-sinnoh-league/34.png
34 - Memories Are Made of Bliss!
/video/13-memories-are-made-of-bliss
'''

Black_White = '''/static/thumbnails/14-black-white/1.png
01 - In the Shadow of Zekrom!
/video/14-in-the-shadow-of-zekrom
/static/thumbnails/14-black-white/2.png
02 - Enter Iris and Axew!
/video/14-enter-iris-and-axew
/static/thumbnails/14-black-white/3.png
03 - A Sandile Gusher of Change!
/video/14-a-sandile-gusher-of-change
/static/thumbnails/14-black-white/4.png
04 - The Battle Club and Tepig's Choice!
/video/14-the-battle-club-and-tepigs-choice
/static/thumbnails/14-black-white/5.png
05 - Triple Leaders, Team Threats!
/video/14-triple-leaders-team-threats
/static/thumbnails/14-black-white/6.png
06 - Dreams by the Yard Full!
/video/14-dreams-by-the-yard-full
/static/thumbnails/14-black-white/7.png
07 - Snivy Plays Hard to Catch!
/video/14-snivy-plays-hard-to-catch
/static/thumbnails/14-black-white/8.png
08 - Saving Darmanitan From the Bell!
/video/14-saving-darmanitan-from-the-bell
/static/thumbnails/14-black-white/9.png
09 - The Bloom Is on Axew!
/video/14-the-bloom-is-on-axew
/static/thumbnails/14-black-white/10.png
10 - A Rival Battle for Club Champ!
/video/14-a-rival-battle-for-club-champ
/static/thumbnails/14-black-white/11.png
11 - A Home for Dwebble!
/video/14-a-home-for-dwebble
/static/thumbnails/14-black-white/12.png
12 - Here Comes the Trubbish Squad!
/video/14-here-comes-the-trubbish-squad
/static/thumbnails/14-black-white/13.png
13 - Minccino-Neat and Tidy!
/video/14-minccino-neat-and-tidy
/static/thumbnails/14-black-white/14.png
14 - A Night in the Nacrene City Museum!
/video/14-a-night-in-the-nacrene-city-museum
/static/thumbnails/14-black-white/15.png
15 - The Battle According to Lenora!
/video/14-the-battle-according-to-lenora
/static/thumbnails/14-black-white/16.png
16 - Rematch at the Nacrene Gym!
/video/14-rematch-at-the-nacrene-gym
/static/thumbnails/14-black-white/17.png
17 - Scraggy-Hatched to Be Wild!
/video/14-scraggy-hatched-to-be-wild
/static/thumbnails/14-black-white/18.png
18 - Sewaddle and Burgh in Pinwheel Forest!
/video/14-sewaddle-and-burgh-in-pinwheel-forest
/static/thumbnails/14-black-white/19.png
19 - A Connoisseur's Revenge!
/video/14-a-connoisseurs-revenge
/static/thumbnails/14-black-white/20.png
20 - Dancing With the Ducklett Trio!
/video/14-dancing-with-the-ducklett-trio
/static/thumbnails/14-black-white/21.png
21 - The Lost World of Gothitelle!
/video/14-the-lost-world-of-gothitelle
/static/thumbnails/14-black-white/22.png
22 - A Venipede Stampede!
/video/14-a-venipede-stampede
/static/thumbnails/14-black-white/23.png
23 - Battling For The Love of Bug-Types!
/video/14-battling-for-the-love-of-bug-types
/static/thumbnails/14-black-white/24.png
24 - Emolga the Irresistible!
/video/14-emolga-the-irresistible
/static/thumbnails/14-black-white/25.png
25 - Emolga and the New Volt Switch!
/video/14-emolga-and-the-new-volt-switch
/static/thumbnails/14-black-white/26.png
26 - Scare at the Litwick Mansion!
/video/14-scare-at-the-litwick-mansion
/static/thumbnails/14-black-white/27.png
27 - The Dragon Master's Path!
/video/14-the-dragon-masters-path
/static/thumbnails/14-black-white/28.png
28 - Oshawott's Lost Scalchop!
/video/14-oshawotts-lost-scalchop
/static/thumbnails/14-black-white/29.png
29 - Cottonee in Love!
/video/14-cottonee-in-love
/static/thumbnails/14-black-white/30.png
30 - A UFO for Elgyem!
/video/14-a-ufo-for-elgyem
/static/thumbnails/14-black-white/31.png
31 - Ash and Trip's Third Battle!
/video/14-ash-and-trips-third-battle
/static/thumbnails/14-black-white/32.png
32 - Facing Fear with Eyes Wide Open!
/video/14-facing-fear-with-eyes-wide-open
/static/thumbnails/14-black-white/33.png
33 - Iris and Excadrill Against the Dragon Buster!
/video/14-iris-and-excadrill-against-the-dragon-buster
/static/thumbnails/14-black-white/34.png
34 - Gotta Catch A Roggenrola!
/video/14-gotta-catch-a-roggenrola
/static/thumbnails/14-black-white/35.png
35 - Where Did You Go, Audino?
/video/14-where-did-you-go-audino
/static/thumbnails/14-black-white/36.png
36 - Archeops In The Modern World!
/video/14-archeops-in-the-modern-world
/static/thumbnails/14-black-white/37.png
37 - A Fishing Connoisseur in a Fishy Competition!
/video/14-a-fishing-connoisseur-in-a-fishy-competition
/static/thumbnails/14-black-white/38.png
38 - Movie Time! Zorua in "The Legend of the Pokemon Knight"!
/video/14-movie-time-zorua-in-the-legend-of-the-pokemon-knight
/static/thumbnails/14-black-white/39.png
39 - Reunion Battles In Nimbasa!
/video/14-reunion-battles-in-nimbasa
/static/thumbnails/14-black-white/40.png
40 - Cilan Versus Trip, Ash Versus Georgia!
/video/14-cilan-versus-trip-ash-versus-georgia
/static/thumbnails/14-black-white/41.png
41 - The Club Battle Hearts of Fury: Emolga Versus Sawk!
/video/14-the-club-battle-hearts-of-fury-emolga-versus-sawk
/static/thumbnails/14-black-white/42.png
42 - Club Battle Finale: A Hero's Outcome!
/video/14-club-battle-finale-a-heros-outcome
/static/thumbnails/14-black-white/43.png
43 - Meowth's Scrafty Tactics!
/video/14-meowths-scrafty-tactics
/static/thumbnails/14-black-white/44.png
44 - Purrloin: Sweet or Sneaky?
/video/14-purrloin-sweet-or-sneaky
/static/thumbnails/14-black-white/45.png
45 - Beheeyem, Duosion, and the Dream Thief!
/video/14-beheeyem-duosion-and-the-dream-thief
/static/thumbnails/14-black-white/46.png
46 - The Beartic Mountain Feud!
/video/14-the-beartic-mountain-feud
/static/thumbnails/14-black-white/47.png
47 - Crisis From the Underground Up!
/video/14-crisis-from-the-underground-up
/static/thumbnails/14-black-white/48.png
48 - Battle for the Underground!
/video/14-battle-for-the-underground'''

BW_Rival_Destinies = '''
/static/thumbnails/15-bw-rival-destinies/1.png
01 - Enter Elesa, Electrifying Gym Leader!
/video/15-enter-elesa-electrifying-gym-leader
/static/thumbnails/15-bw-rival-destinies/2.png
02 - Dazzling the Nimbasa Gym!
/video/15-dazzling-the-nimbasa-gym
/static/thumbnails/15-bw-rival-destinies/3.png
03 - Lost at the Stamp Rally!
/video/15-lost-at-the-stamp-rally
/static/thumbnails/15-bw-rival-destinies/4.png
04 - Ash Versus the Champion!
/video/15-ash-versus-the-champion
/static/thumbnails/15-bw-rival-destinies/5.png
05 - A Maractus Musical!
/video/15-a-maractus-musical
/static/thumbnails/15-bw-rival-destinies/6.png
06 - The Four Seasons of Sawsbuck!
/video/15-the-four-seasons-of-sawsbuck
/static/thumbnails/15-bw-rival-destinies/7.png
07 - Scraggy and the Demanding Gothita!
/video/15-scraggy-and-the-demanding-gothita
/static/thumbnails/15-bw-rival-destinies/8.png
08 - The Lonely Deino!
/video/15-the-lonely-deino
/static/thumbnails/15-bw-rival-destinies/9.png
09 - The Mighty Accelguard to the Rescue!
/video/15-the-mighty-accelguard-to-the-rescue
/static/thumbnails/15-bw-rival-destinies/10.png
10 - A Call for Brotherly Love!
/video/15-a-call-for-brotherly-love
/static/thumbnails/15-bw-rival-destinies/11.png
11 - Stopping the Rage of Legends! Part 1
/video/15-stopping-the-rage-of-legends-part-1
/static/thumbnails/15-bw-rival-destinies/12.png
12 - Stopping the Rage of Legends! Part 2
/video/15-stopping-the-rage-of-legends-part-2
/static/thumbnails/15-bw-rival-destinies/13.png
13 - Battling the King of the Mines!
/video/15-battling-the-king-of-the-mines
/static/thumbnails/15-bw-rival-destinies/14.png
14 - Crisis at Chargestone Cave!
/video/15-crisis-at-chargestone-cave
/static/thumbnails/15-bw-rival-destinies/15.png
15 - Evolution Exchange Excitement!
/video/15-evolution-exchange-excitement
/static/thumbnails/15-bw-rival-destinies/16.png
16 - Explorers of the Hero's Ruin!
/video/15-explorers-of-the-heros-ruin
/static/thumbnails/15-bw-rival-destinies/17.png
17 - Battling the Bully!
/video/15-battling-the-bully
/static/thumbnails/15-bw-rival-destinies/18.png
18 - Baffling the Bouffalant!
/video/15-baffling-the-bouffalant
/static/thumbnails/15-bw-rival-destinies/19.png
19 - Cilan Takes Flight!
/video/15-cilan-takes-flight
/static/thumbnails/15-bw-rival-destinies/20.png
20 - An Amazing Aerial Battle!
/video/15-an-amazing-aerial-battle
/static/thumbnails/15-bw-rival-destinies/21.png
21 - Climbing the Tower of Success!
/video/15-climbing-the-tower-of-success
/static/thumbnails/15-bw-rival-destinies/22.png
22 - The Clubsplosion Begins!
/video/15-the-clubsplosion-begins
/static/thumbnails/15-bw-rival-destinies/23.png
23 - Search for the Clubultimate!
/video/15-search-for-the-clubultimate
/static/thumbnails/15-bw-rival-destinies/24.png
24 - A Clubsplosion of Excitement!
/video/15-a-clubsplosion-of-excitement
/static/thumbnails/15-bw-rival-destinies/25.png
25 - Commanding the Clubsplosion Crown!
/video/15-commanding-the-clubsplosion-crown
/static/thumbnails/15-bw-rival-destinies/26.png
26 - Battling the Leaf Thieves!
/video/15-battling-the-leaf-thieves
/static/thumbnails/15-bw-rival-destinies/27.png
27 - A Restoration Confrontation! Part 1
/video/15-a-restoration-confrontation-part-1
/static/thumbnails/15-bw-rival-destinies/28.png
28 - A Restoration Confrontation! Part 2
/video/15-a-restoration-confrontation-part-2
/static/thumbnails/15-bw-rival-destinies/29.png
29 - Evolution by Fire!
/video/15-evolution-by-fire
/static/thumbnails/15-bw-rival-destinies/30.png
30 - Guarding the Guardian of the Mountain!
/video/15-guarding-the-guardian-of-the-mountain
/static/thumbnails/15-bw-rival-destinies/31.png
31 - Caution: Icy Battle Conditions!
/video/15-caution-icy-battle-conditions
/static/thumbnails/15-bw-rival-destinies/32.png
32 - Clash of the Connoisseurs!
/video/15-clash-of-the-connoisseurs
/static/thumbnails/15-bw-rival-destinies/33.png
33 - Crisis at Ferroseed Research!
/video/15-crisis-at-ferroseed-research
/static/thumbnails/15-bw-rival-destinies/34.png
34 - An Epic Defense Force!
/video/15-an-epic-defense-force
/static/thumbnails/15-bw-rival-destinies/35.png
35 - Rocking the Virbank Gym! Part 1
/video/15-rocking-the-virbank-gym-part-1
/static/thumbnails/15-bw-rival-destinies/36.png
36 - Rocking the Virbank Gym! Part 2
/video/15-rocking-the-virbank-gym-part-2
/static/thumbnails/15-bw-rival-destinies/37.png
37 - All for the Love of Meloetta!
/video/15-all-for-the-love-of-meloetta
/static/thumbnails/15-bw-rival-destinies/38.png
38 - Piplup, Pansage, and a Meeting of the Times!
/video/15-piplup-pansage-and-a-meeting-of-the-times
/static/thumbnails/15-bw-rival-destinies/39.png
39 - Expedition to Onix Island!
/video/15-expedition-to-onix-island
/static/thumbnails/15-bw-rival-destinies/40.png
40 - The Mystery of the Missing Cubchoo!
/video/15-the-mystery-of-the-missing-cubchoo
/static/thumbnails/15-bw-rival-destinies/41.png
41 - Iris and the Rogue Dragonite!
/video/15-iris-and-the-rogue-dragonite
/static/thumbnails/15-bw-rival-destinies/42.png
42 - Jostling for the Junior Cup!
/video/15-jostling-for-the-junior-cup
/static/thumbnails/15-bw-rival-destinies/43.png
43 - Battling Authority Once Again!
/video/15-battling-authority-once-again
/static/thumbnails/15-bw-rival-destinies/44.png
44 - Ash, Iris and Trip: Then There Were Three!
/video/15-ash-iris-and-trip-then-there-were-three
/static/thumbnails/15-bw-rival-destinies/45.png
45 - Goodbye, Junior Cup - Hello Adventure!
/video/15-goodbye-junior-cup-hello-adventure
/static/thumbnails/15-bw-rival-destinies/46.png
46 - The Road to Humilau!
/video/15-the-road-to-humilau
/static/thumbnails/15-bw-rival-destinies/47.png
47 - Unrest at the Nursery!
/video/15-unrest-at-the-nursery
/static/thumbnails/15-bw-rival-destinies/48.png
48 - Meloetta and the Undersea Temple!
/video/15-meloetta-and-the-undersea-temple
/static/thumbnails/15-bw-rival-destinies/49.png
49 - Unova's Survival Crisis!
/video/15-unovas-survival-crisis
'''

BW_Adventures_in_Unova = '''
/static/thumbnails/16-bw-adventures-in-unova/1.png
01 - Beauties Battling for Pride and Prestige!
/video/16-beauties-battling-for-pride-and-prestige
/static/thumbnails/16-bw-adventures-in-unova/2.png
02 - A Surface to Air Tag Battle Team!
/video/16-a-surface-to-air-tag-battle-team
/static/thumbnails/16-bw-adventures-in-unova/3.png
03 - A Village Homecoming!
/video/16-a-village-homecoming
/static/thumbnails/16-bw-adventures-in-unova/4.png
04 - Drayden Versus Iris: Past, Present, and Future!
/video/16-drayden-versus-iris-past-present-and-future
/static/thumbnails/16-bw-adventures-in-unova/5.png
05 - Team Eevee and the Pokemon Rescue Squad!
/video/16-team-eevee-and-the-pokemon-rescue-squad
/static/thumbnails/16-bw-adventures-in-unova/6.png
06 - Curtain Up, Unova League!
/video/16-curtain-up-unova-league
/static/thumbnails/16-bw-adventures-in-unova/7.png
07 - Mission: Defeat Your Rival!
/video/16-mission-defeat-your-rival
/static/thumbnails/16-bw-adventures-in-unova/8.png
08 - Lost at the League!
/video/16-lost-at-the-league
/static/thumbnails/16-bw-adventures-in-unova/9.png
09 - Strong Strategy Steals the Show!
/video/16-strong-strategy-steals-the-show
/static/thumbnails/16-bw-adventures-in-unova/10.png
10 - Cameron's Secret Weapon!
/video/16-camerons-secret-weapon
/static/thumbnails/16-bw-adventures-in-unova/11.png
11 - A Unova League Evolution!
/video/16-a-unova-league-evolution
/static/thumbnails/16-bw-adventures-in-unova/12.png
12 - New Places... Familiar Faces!
/video/16-new-places-familiar-faces
/static/thumbnails/16-bw-adventures-in-unova/13.png
13 - The Name's N!
/video/16-the-names-n
/static/thumbnails/16-bw-adventures-in-unova/14.png
14 - There's a New Gym Leader in Town!
/video/16-theres-a-new-gym-leader-in-town
/static/thumbnails/16-bw-adventures-in-unova/15.png
15 - Team Plasma's Pokemon Power Plot!
/video/16-team-plasmas-pokemon-power-plot
/static/thumbnails/16-bw-adventures-in-unova/16.png
16 - The Light of Floccesy Ranch!
/video/16-the-light-of-floccesy-ranch
/static/thumbnails/16-bw-adventures-in-unova/17.png
17 - Saving Braviary!
/video/16-saving-braviary
/static/thumbnails/16-bw-adventures-in-unova/18.png
18 - The Pokemon Harbor Patrol!
/video/16-the-pokemon-harbor-patrol
/static/thumbnails/16-bw-adventures-in-unova/19.png
19 - The Fires of a Red-Hot Reunion!
/video/16-the-fires-of-a-red-hot-reunion
/static/thumbnails/16-bw-adventures-in-unova/20.png
20 - Team Plasma's Pokemon Manipulation!
/video/16-team-plasmas-pokemon-manipulation
/static/thumbnails/16-bw-adventures-in-unova/21.png
21 - Secrets From Out of the Fog!
/video/16-secrets-from-out-of-the-fog
/static/thumbnails/16-bw-adventures-in-unova/22.png
22 - Meowth, Colress and Team Rivalry!
/video/16-meowth-colress-and-team-rivalry
/static/thumbnails/16-bw-adventures-in-unova/23.png
23 - Ash and N: A Clash of Ideals!
/video/16-ash-and-n-a-clash-of-ideals
/static/thumbnails/16-bw-adventures-in-unova/24.png
24 - Team Plasma and the Awakening Ceremony!
/video/16-team-plasma-and-the-awakening-ceremony
/static/thumbnails/16-bw-adventures-in-unova/25.png
25 - What Lies Beyond Truth and Ideals!
/video/16-what-lies-beyond-truth-and-ideals
/static/thumbnails/16-bw-adventures-in-unova/26.png
26 - Farewell, Unova! Setting Sail for New Adventures!
/video/16-farewell-unova-setting-sail-for-new-adventures
/static/thumbnails/16-bw-adventures-in-unova/27.png
27 - Danger, Sweet as Honey!
/video/16-danger-sweet-as-honey
/static/thumbnails/16-bw-adventures-in-unova/28.png
28 - Cilan and the Case of the Purrloin Witness!
/video/16-cilan-and-the-case-of-the-purrloin-witness
/static/thumbnails/16-bw-adventures-in-unova/29.png
29 - Crowning the Scalchop King!
/video/16-crowning-the-scalchop-king
/static/thumbnails/16-bw-adventures-in-unova/30.png
30 - The Island of Illusions!
/video/16-the-island-of-illusions
/static/thumbnails/16-bw-adventures-in-unova/31.png
31 - To Catch a Rotom!
/video/16-to-catch-a-rotom
/static/thumbnails/16-bw-adventures-in-unova/32.png
32 - The Pirates of Decolore!
/video/16-the-pirates-of-decolore
/static/thumbnails/16-bw-adventures-in-unova/33.png
33 - Butterfree and Me!
/video/16-butterfree-and-me
/static/thumbnails/16-bw-adventures-in-unova/34.png
34 - The Path That Leads to Goodbye!
/video/16-the-path-that-leads-to-goodbye
/static/thumbnails/16-bw-adventures-in-unova/35.png
35 - Searching for a Wish!
/video/16-searching-for-a-wish
/static/thumbnails/16-bw-adventures-in-unova/36.png
36 - Capacia Island UFO!
/video/16-capacia-island-ufo
/static/thumbnails/16-bw-adventures-in-unova/37.png
37 - The Journalist from Another Region!
/video/16-the-journalist-from-another-region
/static/thumbnails/16-bw-adventures-in-unova/38.png
38 - Mystery on a Deserted Island!
/video/16-mystery-on-a-deserted-island
/static/thumbnails/16-bw-adventures-in-unova/39.png
39 - A Pokemon of a Different Color!
/video/16-a-pokemon-of-a-different-color
/static/thumbnails/16-bw-adventures-in-unova/40.png
40 - Celebrating the Hero's Comet!
/video/16-celebrating-the-heros-comet
/static/thumbnails/16-bw-adventures-in-unova/41.png
41 - Go, Go Gogoat!
/video/16-go-go-gogoat
/static/thumbnails/16-bw-adventures-in-unova/42.png
42 - Team Rocket's Shocking Recruit!
/video/16-team-rockets-shocking-recruit
/static/thumbnails/16-bw-adventures-in-unova/43.png
43 - Survival of the Striaton Gym!
/video/16-survival-of-the-striaton-gym
/static/thumbnails/16-bw-adventures-in-unova/44.png
44 - Best Wishes Until We Meet Again!
/video/16-best-wishes-until-we-meet-again
/static/thumbnails/16-bw-adventures-in-unova/45.png
45 - The Dream Continues!
/video/16-the-dream-continues
'''

XY = '''
/static/thumbnails/17-xy/1.png
01 - Kalos, Where Dreams and Adventures Begin!
/video/17-kalos-where-dreams-and-adventures-begin
/static/thumbnails/17-xy/2.png
02 - Lumiose City Pursuit!
/video/17-lumiose-city-pursuit
/static/thumbnails/17-xy/3.png
03 - A Battle of Aerial Mobility!
/video/17-a-battle-of-aerial-mobility
/static/thumbnails/17-xy/4.png
04 - A Shockingly Cheeky Friendship!
/video/17-a-shockingly-cheeky-friendship
/static/thumbnails/17-xy/5.png
05 - A Blustery Santalune Gym Battle!
/video/17-a-blustery-santalune-gym-battle
/static/thumbnails/17-xy/6.png
06 - Battling on Thin Ice!
/video/17-battling-on-thin-ice
/static/thumbnails/17-xy/7.png
07 - Giving Chase at the Rhyhorn Race!
/video/17-giving-chase-at-the-rhyhorn-race
/static/thumbnails/17-xy/8.png
08 - Grooming Furfrou!
/video/17-grooming-furfrou
/static/thumbnails/17-xy/9.png
09 - Clemont's Got a Secret!
/video/17-clemonts-got-a-secret
/static/thumbnails/17-xy/10.png
10 - Mega-Mega Meowth Madness!
/video/17-mega-mega-meowth-madness
/static/thumbnails/17-xy/11.png
11 - The Bamboozling Forest!
/video/17-the-bamboozling-forest
/static/thumbnails/17-xy/12.png
12 - To Catch a Pokemon Smuggler!
/video/17-to-catch-a-pokemon-smuggler
/static/thumbnails/17-xy/13.png
13 - Kindergarten Chaos!
/video/17-kindergarten-chaos
/static/thumbnails/17-xy/14.png
14 - Seeking Shelter From the Storm!
/video/17-seeking-shelter-from-the-storm
/static/thumbnails/17-xy/15.png
15 - An Appetite for Battle!
/video/17-an-appetite-for-battle
/static/thumbnails/17-xy/16.png
16 - A Jolting Switcheroo!
/video/17-a-jolting-switcheroo
/static/thumbnails/17-xy/17.png
17 - A Rush of Ninja Wisdom!
/video/17-a-rush-of-ninja-wisdom
/static/thumbnails/17-xy/18.png
18 - Awakening the Sleeping Giant!
/video/17-awakening-the-sleeping-giant
/static/thumbnails/17-xy/19.png
19 - A Conspiracy to Conquer!
/video/17-a-conspiracy-to-conquer
/static/thumbnails/17-xy/20.png
20 - Breaking Titles at the Chateau!
/video/17-breaking-titles-at-the-chateau
/static/thumbnails/17-xy/21.png
21 - A PokeVision of Things to Come!
/video/17-a-pokevision-of-things-to-come
/static/thumbnails/17-xy/22.png
22 - Going for the Gold!
/video/17-going-for-the-gold
/static/thumbnails/17-xy/23.png
23 - Coming Back into the Cold!
/video/17-coming-back-into-the-cold
/static/thumbnails/17-xy/24.png
24 - Climbing the Walls!
/video/17-climbing-the-walls
/static/thumbnails/17-xy/25.png
25 - A Battle by Any Other Name!
/video/17-a-battle-by-any-other-name
/static/thumbnails/17-xy/26.png
26 - To Find a Fairy Flower!
/video/17-to-find-a-fairy-flower
/static/thumbnails/17-xy/27.png
27 - The Bonds of Evolution!
/video/17-the-bonds-of-evolution
/static/thumbnails/17-xy/28.png
28 - Heroes - Friends and Faux Alike!
/video/17-heroes---friends-and-faux-alike
/static/thumbnails/17-xy/29.png
29 - Mega Revelations!
/video/17-mega-revelations
/static/thumbnails/17-xy/30.png
30 - The Cave of Trials!
/video/17-the-cave-of-trials
/static/thumbnails/17-xy/31.png
31 - The Aura Storm!
/video/17-the-aura-storm
/static/thumbnails/17-xy/32.png
32 - Calling from Beyond the Aura!
/video/17-calling-from-beyond-the-aura
/static/thumbnails/17-xy/33.png
33 - The Bonds of Mega Evolution!
/video/17-the-bonds-of-mega-evolution
/static/thumbnails/17-xy/34.png
34 - The Forest Champion!
/video/17-the-forest-champion
/static/thumbnails/17-xy/35.png
35 - Battles in the Sky!
/video/17-battles-in-the-sky
/static/thumbnails/17-xy/36.png
36 - The Cave of Mirrors!
/video/17-the-cave-of-mirrors
/static/thumbnails/17-xy/37.png
37 - Forging Forest Friendships!
/video/17-forging-forest-friendships
/static/thumbnails/17-xy/38.png
38 - Summer of Discovery!
/video/17-summer-of-discovery
/static/thumbnails/17-xy/39.png
39 - Day Three Blockbusters!
/video/17-day-three-blockbusters
/static/thumbnails/17-xy/40.png
40 - Foggy Pokemon Orienteering!
/video/17-foggy-pokemon-orienteering
/static/thumbnails/17-xy/41.png
41 - Battling Into the Hall of Fame!
/video/17-battling-into-the-hall-of-fame
/static/thumbnails/17-xy/42.png
42 - Origins of Mega Evolution!
/video/17-origins-of-mega-evolution
/static/thumbnails/17-xy/43.png
43 - Showdown at the Shalour Gym!
/video/17-showdown-at-the-shalour-gym
/static/thumbnails/17-xy/44.png
44 - Splitting Heirs!
/video/17-splitting-heirs
/static/thumbnails/17-xy/45.png
45 - The Clumsy Crier Quiets the Chaos!
/video/17-the-clumsy-crier-quiets-the-chaos
/static/thumbnails/17-xy/46.png
46 - Dreaming a Performer's Dream!
/video/17-dreaming-a-performers-dream
/static/thumbnails/17-xy/47.png
47 - A Campus Reunion!
/video/17-a-campus-reunion
/static/thumbnails/17-xy/48.png
48 - Bonnie for the Defense!
/video/17-bonnie-for-the-defense
'''

XY_Kalos_Quest = '''
/static/thumbnails/18-xy-kalos-quest/1.png
01 - Pathways to Performance Partnering!
/video/18-pathways-to-performance-partnering
/static/thumbnails/18-xy-kalos-quest/2.png
02 - When Light and Dark Collide!
/video/18-when-light-and-dark-collide
/static/thumbnails/18-xy-kalos-quest/3.png
03 - An Undersea Place to Call Home!
/video/18-an-undersea-place-to-call-home
/static/thumbnails/18-xy-kalos-quest/4.png
04 - A Stealthy Challenge!
/video/18-a-stealthy-challenge
/static/thumbnails/18-xy-kalos-quest/5.png
05 - A Race for Home!
/video/18-a-race-for-home
/static/thumbnails/18-xy-kalos-quest/6.png
06 - Facing the Grand Design!
/video/18-facing-the-grand-design
/static/thumbnails/18-xy-kalos-quest/7.png
07 - A Slippery Encounter!
/video/18-a-slippery-encounter
/static/thumbnails/18-xy-kalos-quest/8.png
08 - One for the Goomy!
/video/18-one-for-the-goomy
/static/thumbnails/18-xy-kalos-quest/9.png
09 - Thawing an Icy Panic!
/video/18-thawing-an-icy-panic
/static/thumbnails/18-xy-kalos-quest/10.png
10 - The Green, Green Grass Types of Home!
/video/18-the-green-green-grass-types-of-home
/static/thumbnails/18-xy-kalos-quest/11.png
11 - Under the Pledging Tree!
/video/18-under-the-pledging-tree
/static/thumbnails/18-xy-kalos-quest/12.png
12 - A Showcase Debut!
/video/18-a-showcase-debut
/static/thumbnails/18-xy-kalos-quest/13.png
13 - An Oasis of Hope!
/video/18-an-oasis-of-hope
/static/thumbnails/18-xy-kalos-quest/14.png
14 - The Future Is Now, Thanks to Determination!
/video/18-the-future-is-now-thanks-to-determination
/static/thumbnails/18-xy-kalos-quest/15.png
15 - A Fork in the Road! A Parting of the Ways!
/video/18-a-fork-in-the-road-a-parting-of-the-ways
/static/thumbnails/18-xy-kalos-quest/16.png
16 - Battling with Elegance and a Big Smile!
/video/18-battling-with-elegance-and-a-big-smile
/static/thumbnails/18-xy-kalos-quest/17.png
17 - Good Friends, Great Training!
/video/18-good-friends-great-training
/static/thumbnails/18-xy-kalos-quest/18.png
18 - Confronting the Darkness!
/video/18-confronting-the-darkness
/static/thumbnails/18-xy-kalos-quest/19.png
19 - The Moment of Lumiose Truth!
/video/18-the-moment-of-lumiose-truth
/static/thumbnails/18-xy-kalos-quest/20.png
20 - Garchomp's Mega Bond!
/video/18-garchomps-mega-bond
/static/thumbnails/18-xy-kalos-quest/21.png
21 - Defending the Homeland!
/video/18-defending-the-homeland
/static/thumbnails/18-xy-kalos-quest/22.png
22 - Beyond the Rainbow!
/video/18-beyond-the-rainbow
/static/thumbnails/18-xy-kalos-quest/23.png
23 - So You're Having a Bad Day!
/video/18-so-youre-having-a-bad-day
/static/thumbnails/18-xy-kalos-quest/24.png
24 - Scary Hospitality!
/video/18-scary-hospitality
/static/thumbnails/18-xy-kalos-quest/25.png
25 - A Fashionable Battle!
/video/18-a-fashionable-battle
/static/thumbnails/18-xy-kalos-quest/26.png
26 - Fairy-Type Trickery!
/video/18-fairy-type-trickery
/static/thumbnails/18-xy-kalos-quest/27.png
27 - Rivals: Today and Tomorrow!
/video/18-rivals-today-and-tomorrow
/static/thumbnails/18-xy-kalos-quest/28.png
28 - A Not-So-Flying Start!
/video/18-a-not-so-flying-start
/static/thumbnails/18-xy-kalos-quest/29.png
29 - A Relay in the Sky!
/video/18-a-relay-in-the-sky
/static/thumbnails/18-xy-kalos-quest/30.png
30 - A Frenzied Factory Fiasco!
/video/18-a-frenzied-factory-fiasco
/static/thumbnails/18-xy-kalos-quest/31.png
31 - Performing with Fiery Charm!
/video/18-performing-with-fiery-charm
/static/thumbnails/18-xy-kalos-quest/32.png
32 - Rotom's Wish!
/video/18-rotoms-wish
/static/thumbnails/18-xy-kalos-quest/33.png
33 - A Festival Trade! A Festival Farewell?
/video/18-a-festival-trade-a-festival-farewell
/static/thumbnails/18-xy-kalos-quest/34.png
34 - Over the Mountain of Snow!
/video/18-over-the-mountain-of-snow
/static/thumbnails/18-xy-kalos-quest/35.png
35 - Adventures in Running Errands!
/video/18-adventures-in-running-errands
/static/thumbnails/18-xy-kalos-quest/36.png
36 - Mending a Broken Spirit!
/video/18-mending-a-broken-spirit
/static/thumbnails/18-xy-kalos-quest/37.png
37 - A Legendary Photo Op!
/video/18-a-legendary-photo-op
/static/thumbnails/18-xy-kalos-quest/38.png
38 - The Tiny Caretaker!
/video/18-the-tiny-caretaker
/static/thumbnails/18-xy-kalos-quest/39.png
39 - A Trip Down Memory Train!
/video/18-a-trip-down-memory-train
/static/thumbnails/18-xy-kalos-quest/40.png
40 - A Frolicking Find in the Flowers!
/video/18-a-frolicking-find-in-the-flowers
/static/thumbnails/18-xy-kalos-quest/41.png
41 - Lights! Camera! Pika!
/video/18-lights-camera-pika
/static/thumbnails/18-xy-kalos-quest/42.png
42 - Tag Team Battle Inspiration!
/video/18-tag-team-battle-inspiration
/static/thumbnails/18-xy-kalos-quest/43.png
43 - A Performance Pop Quiz!
/video/18-a-performance-pop-quiz
/static/thumbnails/18-xy-kalos-quest/44.png
44 - Cloudy Fate, Bright Future!
/video/18-cloudy-fate-bright-future
/static/thumbnails/18-xy-kalos-quest/45.png
45 - All Eyes on the Future!
/video/18-all-eyes-on-the-future
'''

XYZ = '''
/static/thumbnails/19-xyz/1.png
01 - From A to Z!
/video/19-from-a-to-z
/static/thumbnails/19-xyz/2.png
02 - Love Strikes! Eevee, Yikes!
/video/19-love-strikes-eevee-yikes
/static/thumbnails/19-xyz/3.png
03 - A Giga Battle with Mega Results!
/video/19-a-giga-battle-with-mega-results
/static/thumbnails/19-xyz/4.png
04 - A Fiery Rite of Passage!
/video/19-a-fiery-rite-of-passage
/static/thumbnails/19-xyz/5.png
05 - Dream a Little Dream from Me!
/video/19-dream-a-little-dream-from-me
/static/thumbnails/19-xyz/6.png
06 - The Legend of the Ninja Hero!
/video/19-the-legend-of-the-ninja-hero
/static/thumbnails/19-xyz/7.png
07 - A Festival of Decisions!
/video/19-a-festival-of-decisions
/static/thumbnails/19-xyz/8.png
08 - A Dancing Debut!
/video/19-a-dancing-debut
/static/thumbnails/19-xyz/9.png
09 - Meeting at Terminus Cave!
/video/19-meeting-at-terminus-cave
/static/thumbnails/19-xyz/10.png
10 - A Cellular Connection!
/video/19-a-cellular-connection
/static/thumbnails/19-xyz/11.png
11 - A Windswept Encounter!
/video/19-a-windswept-encounter
/static/thumbnails/19-xyz/12.png
12 - Party Dancecapades!
/video/19-party-dancecapades
/static/thumbnails/19-xyz/13.png
13 - A Meeting of Two Journeys!
/video/19-a-meeting-of-two-journeys
/static/thumbnails/19-xyz/14.png
14 - An Explosive Operation!
/video/19-an-explosive-operation
/static/thumbnails/19-xyz/15.png
15 - A Watershed Moment!
/video/19-a-watershed-moment
/static/thumbnails/19-xyz/16.png
16 - Master Class Choices!
/video/19-master-class-choices
/static/thumbnails/19-xyz/17.png
17 - An Electrifying Rage!
/video/19-an-electrifying-rage
/static/thumbnails/19-xyz/18.png
18 - Unlocking Some Respect!
/video/19-unlocking-some-respect
/static/thumbnails/19-xyz/19.png
19 - Master Class is in Session!
/video/19-master-class-is-in-session
/static/thumbnails/19-xyz/20.png
20 - Performing a Pathway to the Future!
/video/19-performing-a-pathway-to-the-future
/static/thumbnails/19-xyz/21.png
21 - A Keeper for Keeps?
/video/19-a-keeper-for-keeps
/static/thumbnails/19-xyz/22.png
22 - Battling at Full Volume!
/video/19-battling-at-full-volume
/static/thumbnails/19-xyz/23.png
23 - The Synchronicity Test!
/video/19-the-synchronicity-test
/static/thumbnails/19-xyz/24.png
24 - Making Friends and Influencing Villains!
/video/19-making-friends-and-influencing-villains
/static/thumbnails/19-xyz/25.png
25 - Championing a Research Battle!
/video/19-championing-a-research-battle
/static/thumbnails/19-xyz/26.png
26 - A Full-Strength Battle Surprise!
/video/19-a-full-strength-battle-surprise
/static/thumbnails/19-xyz/27.png
27 - All Hail the Ice Battlefield!
/video/19-all-hail-the-ice-battlefield
/static/thumbnails/19-xyz/28.png
28 - Seeing the Forest for the Trees!
/video/19-seeing-the-forest-for-the-trees
/static/thumbnails/19-xyz/29.png
29 - A Real Icebreaker!
/video/19-a-real-icebreaker
/static/thumbnails/19-xyz/30.png
30 - A Diamond in the Rough!
/video/19-a-diamond-in-the-rough
/static/thumbnails/19-xyz/31.png
31 - A Gaggle of Gadget Greatness!
/video/19-a-gaggle-of-gadget-greatness
/static/thumbnails/19-xyz/32.png
32 - A League of His Own!
/video/19-a-league-of-his-own
/static/thumbnails/19-xyz/33.png
33 - Valuable Experience for All!
/video/19-valuable-experience-for-all
/static/thumbnails/19-xyz/34.png
34 - Analysis Versus Passion!
/video/19-analysis-versus-passion
/static/thumbnails/19-xyz/35.png
35 - A Riveting Rivalry!
/video/19-a-riveting-rivalry
/static/thumbnails/19-xyz/36.png
36 - Kalos League Passion with a Certain Flare!
/video/19-kalos-league-passion-with-a-certain-flare
/static/thumbnails/19-xyz/37.png
37 - Finals Not for the Faint-Hearted!
/video/19-finals-not-for-the-faint-hearted
/static/thumbnails/19-xyz/38.png
38 - Down to the Fiery Finish!
/video/19-down-to-the-fiery-finish
/static/thumbnails/19-xyz/39.png
39 - A Towering Takeover!
/video/19-a-towering-takeover
/static/thumbnails/19-xyz/40.png
40 - Coming Apart at the Dreams!
/video/19-coming-apart-at-the-dreams
/static/thumbnails/19-xyz/41.png
41 - The Right Hero for the Right Job!
/video/19-the-right-hero-for-the-right-job
/static/thumbnails/19-xyz/42.png
42 - Rocking Kalos Defenses!
/video/19-rocking-kalos-defenses
/static/thumbnails/19-xyz/43.png
43 - Forming a More Perfect Union!
/video/19-forming-a-more-perfect-union
/static/thumbnails/19-xyz/44.png
44 - Battling With a Clean Slate!
/video/19-battling-with-a-clean-slate
/static/thumbnails/19-xyz/45.png
45 - The First Day of the Rest of Your Life!
/video/19-the-first-day-of-the-rest-of-your-life
/static/thumbnails/19-xyz/46.png
46 - Facing the Needs of the Many!
/video/19-facing-the-needs-of-the-many
/static/thumbnails/19-xyz/47.png
47 - Till We Compete Again!
/video/19-till-we-compete-again
/static/thumbnails/19-xyz/48.png
48 - The Legend of X, Y, and Z!
/video/19-the-legend-of-x-y-and-z
'''

Sun_Moon = '''
/static/thumbnails/20-sun-moon/1.png
01 - Alola to New Adventure!
/video/20-alola-to-new-adventure
/static/thumbnails/20-sun-moon/2.png
02 - The Guardian's Challenge!
/video/20-the-guardians-challenge
/static/thumbnails/20-sun-moon/3.png
03 - Loading the Dex!
/video/20-loading-the-dex
/static/thumbnails/20-sun-moon/4.png
04 - First Catch in Alola, Ketchum-Style!
/video/20-first-catch-in-alola-ketchum-style
/static/thumbnails/20-sun-moon/5.png
05 - Yo, Ho, Ho! Go, Popplio!
/video/20-yo-ho-ho-go-popplio
/static/thumbnails/20-sun-moon/6.png
06 - A Shocking Grocery Run!
/video/20-a-shocking-grocery-run
/static/thumbnails/20-sun-moon/7.png
07 - That's Why the Litten is a Scamp!
/video/20-thats-why-the-litten-is-a-scamp
/static/thumbnails/20-sun-moon/8.png
08 - Lillie's Egg-xhilarating Challenge!
/video/20-lillies-egg-xhilarating-challenge
/static/thumbnails/20-sun-moon/9.png
09 - To Top a Totem!
/video/20-to-top-a-totem
/static/thumbnails/20-sun-moon/10.png
10 - Trial and Tribulation!
/video/20-trial-and-tribulation
/static/thumbnails/20-sun-moon/11.png
11 - Young Kiawe Had a Farm!
/video/20-young-kiawe-had-a-farm
/static/thumbnails/20-sun-moon/12.png
12 - The Sun, the Scare, the Secret Lair!
/video/20-the-sun-the-scare-the-secret-lair
/static/thumbnails/20-sun-moon/13.png
13 - Racing to a Big Event!
/video/20-racing-to-a-big-event
/static/thumbnails/20-sun-moon/14.png
14 - Getting to Know You!
/video/20-getting-to-know-you
/static/thumbnails/20-sun-moon/15.png
15 - Rocking Clawmark Hill!
/video/20-rocking-clawmark-hill
/static/thumbnails/20-sun-moon/16.png
16 - They Might Not Be Giants!
/video/20-they-might-not-be-giants
/static/thumbnails/20-sun-moon/17.png
17 - Crystal-Clear Sleuthing!
/video/20-crystal-clear-sleuthing
/static/thumbnails/20-sun-moon/18.png
18 - A Seasoned Search!
/video/20-a-seasoned-search
/static/thumbnails/20-sun-moon/19.png
19 - A Guardian Rematch!
/video/20-a-guardian-rematch
/static/thumbnails/20-sun-moon/20.png
20 - Partner Promises!
/video/20-partner-promises
/static/thumbnails/20-sun-moon/21.png
21 - One Journey Ends, Another Begins...
/video/20-one-journey-ends-another-begins
/static/thumbnails/20-sun-moon/22.png
22 - A Shivering Shovel Search!
/video/20-a-shivering-shovel-search
/static/thumbnails/20-sun-moon/23.png
23 - Getting the Band Back Together!
/video/20-getting-the-band-back-together
/static/thumbnails/20-sun-moon/24.png
24 - Alolan Open House!
/video/20-alolan-open-house
/static/thumbnails/20-sun-moon/25.png
25 - A Team-on-Team Tussle!
/video/20-a-team-on-team-tussle
/static/thumbnails/20-sun-moon/26.png
26 - So Long, Sophocles!
/video/20-so-long-sophocles
/static/thumbnails/20-sun-moon/27.png
27 - A Glaring Rivalry!
/video/20-a-glaring-rivalry
/static/thumbnails/20-sun-moon/28.png
28 - Pulling Out the Pokemon Base Pepper!
/video/20-pulling-out-the-pokemon-base-pepper
/static/thumbnails/20-sun-moon/29.png
29 - Lulled to La-La Land!
/video/20-lulled-to-la-la-land
/static/thumbnails/20-sun-moon/30.png
30 - The Ol' Raise and Switch!
/video/20-the-ol-raise-and-switch
/static/thumbnails/20-sun-moon/31.png
31 - The Island Whisperer!
/video/20-the-island-whisperer
/static/thumbnails/20-sun-moon/32.png
32 - Treasure Hunt, Akala Style!
/video/20-treasure-hunt-akala-style
/static/thumbnails/20-sun-moon/33.png
33 - Big Sky, Small Fry!
/video/20-big-sky-small-fry
/static/thumbnails/20-sun-moon/34.png
34 - A Crowning Moment of Truth!
/video/20-a-crowning-moment-of-truth
/static/thumbnails/20-sun-moon/35.png
35 - Currying Favor and Flavor!
/video/20-currying-favor-and-flavor
/static/thumbnails/20-sun-moon/36.png
36 - Trials and Determinations!
/video/20-trials-and-determinations
/static/thumbnails/20-sun-moon/37.png
37 - Rising from the Ruins!
/video/20-rising-from-the-ruins
/static/thumbnails/20-sun-moon/38.png
38 - Mimikyu Unmasked!
/video/20-mimikyu-unmasked
/static/thumbnails/20-sun-moon/39.png
39 - Mallow and the Forest Teacher!
/video/20-mallow-and-the-forest-teacher
/static/thumbnails/20-sun-moon/40.png
40 - Balloons, Brionne, and Belligerence!
/video/20-balloons-brionne-and-belligerence
/static/thumbnails/20-sun-moon/41.png
41 - Mounting an Electrifying Charge!
/video/20-mounting-an-electrifying-charge
/static/thumbnails/20-sun-moon/42.png
42 - Alola, Kanto!
/video/20-alola-kanto
/static/thumbnails/20-sun-moon/43.png
43 - When Regions Collide!
/video/20-when-regions-collide
'''

Sun_Moon_Ultra_Adventure = '''
/static/thumbnails/21-sun-moon-ultra-adventures/1.png
01 - A Dream Encounter!
/video/21-a-dream-encounter
/static/thumbnails/21-sun-moon-ultra-adventures/2.png
02 - Now You See Them, Now You Don't!
/video/21-now-you-see-them-now-you-dont
/static/thumbnails/21-sun-moon-ultra-adventures/3.png
03 - Deceiving Appearances!
/video/21-deceiving-appearances
/static/thumbnails/21-sun-moon-ultra-adventures/4.png
04 - A Masked Warning!
/video/21-a-masked-warning
/static/thumbnails/21-sun-moon-ultra-adventures/5.png
05 - Night of a Thousand Poses!
/video/21-night-of-a-thousand-poses
/static/thumbnails/21-sun-moon-ultra-adventures/6.png
06 - Mission: Total Recall!
/video/21-mission-total-recall
/static/thumbnails/21-sun-moon-ultra-adventures/7.png
07 - Faba's Revenge!
/video/21-fabas-revenge
/static/thumbnails/21-sun-moon-ultra-adventures/8.png
08 - Family Determination!
/video/21-family-determination
/static/thumbnails/21-sun-moon-ultra-adventures/9.png
09 - Revealing the Stuff of Legend!
/video/21-revealing-the-stuff-of-legend
/static/thumbnails/21-sun-moon-ultra-adventures/10.png
10 - Rescuing the Unwilling!
/video/21-rescuing-the-unwilling
/static/thumbnails/21-sun-moon-ultra-adventures/11.png
11 - 10,000,000 Reasons to Fight!
/video/21-10000000-reasons-to-fight
/static/thumbnails/21-sun-moon-ultra-adventures/12.png
12 - The Professors' New Adventure!
/video/21-the-professors-new-adventure
/static/thumbnails/21-sun-moon-ultra-adventures/13.png
13 - Let Sleeping Pokemon Lie!
/video/21-let-sleeping-pokemon-lie
/static/thumbnails/21-sun-moon-ultra-adventures/14.png
14 - The Dex Can't Help It!
/video/21-the-dex-cant-help-it
/static/thumbnails/21-sun-moon-ultra-adventures/15.png
15 - Fighting Back the Tears!
/video/21-fighting-back-the-tears
/static/thumbnails/21-sun-moon-ultra-adventures/16.png
16 - Tasting the Bitter with the Sweet!
/video/21-tasting-the-bitter-with-the-sweet
/static/thumbnails/21-sun-moon-ultra-adventures/17.png
17 - Getting a Jump on the Competition!
/video/21-getting-a-jump-on-the-competition
/static/thumbnails/21-sun-moon-ultra-adventures/18.png
18 - A Mission of Ultra Urgency!
/video/21-a-mission-of-ultra-urgency
/static/thumbnails/21-sun-moon-ultra-adventures/19.png
19 - Acting True to Form!
/video/21-acting-true-to-form
/static/thumbnails/21-sun-moon-ultra-adventures/20.png
20 - Pushing the Fiery Envelope!
/video/21-pushing-the-fiery-envelope
/static/thumbnails/21-sun-moon-ultra-adventures/21.png
21 - Satoshi and Nagetukesaru! Touchdown of Friendship!!
/video/21-satoshi-and-nagetukesaru-touchdown-of-friendship
/static/thumbnails/21-sun-moon-ultra-adventures/22.png
22 - Turning Heads and Training Hard!
/video/21-turning-heads-and-training-hard
/static/thumbnails/21-sun-moon-ultra-adventures/23.png
23 - Smashing with Sketch!
/video/21-smashing-with-sketch
/static/thumbnails/21-sun-moon-ultra-adventures/24.png
24 - Love at First Twirl!
/video/21-love-at-first-twirl
/static/thumbnails/21-sun-moon-ultra-adventures/25.png
25 - Real Life...Inquire Within!
/video/21-real-life-inquire-within
/static/thumbnails/21-sun-moon-ultra-adventures/26.png
26 - Rise and Shine, Starship!
/video/21-rise-and-shine-starship
/static/thumbnails/21-sun-moon-ultra-adventures/27.png
27 - The Young Flame Strikes Back!
/video/21-the-young-flame-strikes-back
/static/thumbnails/21-sun-moon-ultra-adventures/28.png
28 - Dewpider Ascending!
/video/21-dewpider-ascending
/static/thumbnails/21-sun-moon-ultra-adventures/29.png
29 - Sours for the Sweet!
/video/21-sours-for-the-sweet
/static/thumbnails/21-sun-moon-ultra-adventures/30.png
30 - Why Not Give Me a Z-Ring Sometime?
/video/21-why-not-give-me-a-z-ring-sometime
/static/thumbnails/21-sun-moon-ultra-adventures/31.png
31 - Tough Guy Trials!
/video/21-tough-guy-trials
/static/thumbnails/21-sun-moon-ultra-adventures/32.png
32 - Some Kind of Laziness!
/video/21-some-kind-of-laziness
/static/thumbnails/21-sun-moon-ultra-adventures/33.png
33 - A Battle Hand-Off!
/video/21-a-battle-hand-off
/static/thumbnails/21-sun-moon-ultra-adventures/34.png
34 - Guiding an Awakening!
/video/21-guiding-an-awakening
/static/thumbnails/21-sun-moon-ultra-adventures/35.png
35 - Twirling with a Bang!
/video/21-twirling-with-a-bang
/static/thumbnails/21-sun-moon-ultra-adventures/36.png
36 - Showering the World with Love!
/video/21-showering-the-world-with-love
/static/thumbnails/21-sun-moon-ultra-adventures/37.png
37 - Not Caving Under Pressure!
/video/21-not-caving-under-pressure
/static/thumbnails/21-sun-moon-ultra-adventures/38.png
38 - A Young Royal Flame Ignites!
/video/21-a-young-royal-flame-ignites
/static/thumbnails/21-sun-moon-ultra-adventures/39.png
39 - All They Want to Do is Dance Dance!
/video/21-all-they-want-to-do-is-dance-dance
/static/thumbnails/21-sun-moon-ultra-adventures/40.png
40 - Dummy, You Shrunk the Kids!
/video/21-dummy-you-shrunk-the-kids
/static/thumbnails/21-sun-moon-ultra-adventures/41.png
41 - The Shape of Love to Come!
/video/21-the-shape-of-love-to-come
/static/thumbnails/21-sun-moon-ultra-adventures/42.png
42 - The Long Vault Home!
/video/21-the-long-vault-home
/static/thumbnails/21-sun-moon-ultra-adventures/43.png
43 - I Choose Paradise!
/video/21-i-choose-paradise
/static/thumbnails/21-sun-moon-ultra-adventures/44.png
44 - Filling the Light with Darkness!
/video/21-filling-the-light-with-darkness
/static/thumbnails/21-sun-moon-ultra-adventures/45.png
45 - Full Moon and Many Arms!
/video/21-full-moon-and-many-arms
/static/thumbnails/21-sun-moon-ultra-adventures/46.png
46 - The Prism Between Light and Darkness!
/video/21-the-prism-between-light-and-darkness
/static/thumbnails/21-sun-moon-ultra-adventures/47.png
47 - Securing the Future!
/video/21-securing-the-future
/static/thumbnails/21-sun-moon-ultra-adventures/48.png
48 - A Plethora of Pikachu!
/video/21-a-plethora-of-pikachu
/static/thumbnails/21-sun-moon-ultra-adventures/49.png
49 - Turning the Other Mask!
/video/21-turning-the-other-mask
'''

Sun_Moon_Ultra_Legend = '''
/static/thumbnails/22-sun-moon-ultra-legends/1.png
01 - Lillier and the Staff!
/video/22-lillier-and-the-staff
/static/thumbnails/22-sun-moon-ultra-legends/2.png
02 - A Haunted House for Everyone!
/video/22-a-haunted-house-for-everyone
/static/thumbnails/22-sun-moon-ultra-legends/3.png
03 - Sparking Confusion!
/video/22-sparking-confusion
/static/thumbnails/22-sun-moon-ultra-legends/4.png
04 - Don't Ignore the Small Stufful!
/video/22-dont-ignore-the-small-stufful
/static/thumbnails/22-sun-moon-ultra-legends/5.png
05 - No Stone Unturned!
/video/22-no-stone-unturned
/static/thumbnails/22-sun-moon-ultra-legends/6.png
06 - Bright Lights, Big Changes!
/video/22-bright-lights-big-changes
/static/thumbnails/22-sun-moon-ultra-legends/7.png
07 - We Know Where You're Going, Eevee!
/video/22-we-know-where-youre-going-eevee
/static/thumbnails/22-sun-moon-ultra-legends/8.png
08 - Battling the Beast Within!
/video/22-battling-the-beast-within
/static/thumbnails/22-sun-moon-ultra-legends/9.png
09 - Parallel Friendships!
/video/22-parallel-friendships
/static/thumbnails/22-sun-moon-ultra-legends/10.png
10 - Alola, Alola!
/video/22-alola-alola
/static/thumbnails/22-sun-moon-ultra-legends/11.png
11 - Heart of Fire! Heart of Stone!
/video/22-heart-of-fire-heart-of-stone
/static/thumbnails/22-sun-moon-ultra-legends/12.png
12 - That's Some Spicy Island Research!
/video/22-thats-some-spicy-island-research
/static/thumbnails/22-sun-moon-ultra-legends/13.png
13 - Showdown on Poni Island!
/video/22-showdown-on-poni-island
/static/thumbnails/22-sun-moon-ultra-legends/14.png
14 - Evolving Research!
/video/22-evolving-research
/static/thumbnails/22-sun-moon-ultra-legends/15.png
15 - Run, Heroes, Run!
/video/22-run-heroes-run
/static/thumbnails/22-sun-moon-ultra-legends/16.png
16 - Memories in the Mist!
/video/22-memories-in-the-mist
/static/thumbnails/22-sun-moon-ultra-legends/17.png
17 - A Grand Debut!
/video/22-a-grand-debut
/static/thumbnails/22-sun-moon-ultra-legends/18.png
18 - Keeping Your Eyes on the Ball!
/video/22-keeping-your-eyes-on-the-ball
/static/thumbnails/22-sun-moon-ultra-legends/19.png
19 - Show Me the Metal!
/video/22-show-me-the-metal
/static/thumbnails/22-sun-moon-ultra-legends/20.png
20 - Got Meltan?
/video/22-got-meltan
/static/thumbnails/22-sun-moon-ultra-legends/21.png
21 - This Magik Moment!
/video/22-this-magik-moment
/static/thumbnails/22-sun-moon-ultra-legends/22.png
22 - Beauty is Only Crystal Deep!
/video/22-beauty-is-only-crystal-deep
/static/thumbnails/22-sun-moon-ultra-legends/23.png
23 - The Dealer of Destruction!
/video/22-the-dealer-of-destruction
/static/thumbnails/22-sun-moon-ultra-legends/24.png
24 - The Secret Princess!
/video/22-the-secret-princess
/static/thumbnails/22-sun-moon-ultra-legends/25.png
25 - Drawn with the Wind!
/video/22-drawn-with-the-wind
/static/thumbnails/22-sun-moon-ultra-legends/26.png
26 - Aiming for the Top Floor!
/video/22-aiming-for-the-top-floor
/static/thumbnails/22-sun-moon-ultra-legends/27.png
27 - A High-Speed Awakening!
/video/22-a-high-speed-awakening
/static/thumbnails/22-sun-moon-ultra-legends/28.png
28 - The One That Didn't Get Away!
/video/22-the-one-that-didnt-get-away
/static/thumbnails/22-sun-moon-ultra-legends/29.png
29 - A Recipe for Success!
/video/22-a-recipe-for-success
/static/thumbnails/22-sun-moon-ultra-legends/30.png
30 - Spying for the Big Guy!
/video/22-spying-for-the-big-guy
/static/thumbnails/22-sun-moon-ultra-legends/31.png
31 - A Fiery Training Camp Trick!
/video/22-a-fiery-training-camp-trick
/static/thumbnails/22-sun-moon-ultra-legends/32.png
32 - Living on the Cutting Edge!
/video/22-living-on-the-cutting-edge
/static/thumbnails/22-sun-moon-ultra-legends/33.png
33 - A Timeless Encounter!
/video/22-a-timeless-encounter
/static/thumbnails/22-sun-moon-ultra-legends/34.png
34 - Pikachu's Exciting Adventure!
/video/22-pikachus-exciting-adventure
/static/thumbnails/22-sun-moon-ultra-legends/35.png
35 - Chasing Memories, Creating Dreams!
/video/22-chasing-memories-creating-dreams
/static/thumbnails/22-sun-moon-ultra-legends/36.png
36 - League Offenders and Defenders!
/video/22-league-offenders-and-defenders
/static/thumbnails/22-sun-moon-ultra-legends/37.png
37 - Battle Royal 151!
/video/22-battle-royal-151
/static/thumbnails/22-sun-moon-ultra-legends/38.png
38 - Battling Besties!
/video/22-battling-besties
/static/thumbnails/22-sun-moon-ultra-legends/39.png
39 - The Battlefield of Truth and Love!
/video/22-the-battlefield-of-truth-and-love
/static/thumbnails/22-sun-moon-ultra-legends/40.png
40 - Imitation Is the Sincerest Form of Strategy!
/video/22-imitation-is-the-sincerest-form-of-strategy
/static/thumbnails/22-sun-moon-ultra-legends/41.png
41 - Battling on the Wing!
/video/22-battling-on-the-wing
/static/thumbnails/22-sun-moon-ultra-legends/42.png
42 - The Road to the Semifinals!
/video/22-the-road-to-the-semifinals
/static/thumbnails/22-sun-moon-ultra-legends/43.png
43 - The Final Four!
/video/22-the-final-four
/static/thumbnails/22-sun-moon-ultra-legends/44.png
44 - Getting Down to The Ire!
/video/22-getting-down-to-the-ire
/static/thumbnails/22-sun-moon-ultra-legends/45.png
45 - The Wisdom Not to Run!a_Z
/video/22-the-wisdom-not-to-run
/static/thumbnails/22-sun-moon-ultra-legends/46.png
46 - Final Rivals!
/video/22-final-rivals
/static/thumbnails/22-sun-moon-ultra-legends/47.png
47 - Enter the Champion!
/video/22-enter-the-champion
/static/thumbnails/22-sun-moon-ultra-legends/48.png
48 - Z-Move Showdown!
/video/22-z-move-showdown
/static/thumbnails/22-sun-moon-ultra-legends/49.png
49 - Exhibition Unmasked!
/video/22-exhibition-unmasked
/static/thumbnails/22-sun-moon-ultra-legends/50.png
50 - A Full Battle Bounty!
/video/22-a-full-battle-bounty
/static/thumbnails/22-sun-moon-ultra-legends/51.png
51 - Fiery Surprises!
/video/22-fiery-surprises
/static/thumbnails/22-sun-moon-ultra-legends/52.png
52 - From Z to Shining Z!
/video/22-from-z-to-shining-z
/static/thumbnails/22-sun-moon-ultra-legends/53.png
53 - Dreaming of the Sun and Moon!
/video/22-dreaming-of-the-sun-and-moon
/static/thumbnails/22-sun-moon-ultra-legends/54.png
54 - Thank You, Alola! The Journey Continues!
/video/22-thank-you-alola-the-journey-continues
'''

Journeys = '''
/static/thumbnails/23-journeys/1.png
01 - Enter Pikachu!
/video/23-enter-pikachu
/static/thumbnails/23-journeys/2.png
02 - Legend? Go! Friends? Go!
/video/23-legend-go-friends-go
/static/thumbnails/23-journeys/3.png
03 - Ivysaur's Mysterious Tower!
/video/23-ivysaurs-mysterious-tower
/static/thumbnails/23-journeys/4.png
04 - Settling the Scorbunny!
/video/23-settling-the-scorbunny
/static/thumbnails/23-journeys/5.png
05 - Mind-Boggling Dynamax!
/video/23-mind-boggling-dynamax
/static/thumbnails/23-journeys/6.png
06 - Working My Way Back to Mew!
/video/23-working-my-way-back-to-mew
/static/thumbnails/23-journeys/7.png
07 - Serving Up the Flute Cup!
/video/23-serving-up-the-flute-cup
/static/thumbnails/23-journeys/8.png
08 - The Sinnoh Iceberg Race!
/video/23-the-sinnoh-iceberg-race
/static/thumbnails/23-journeys/9.png
09 - Finding a Legend!
/video/23-finding-a-legend
/static/thumbnails/23-journeys/10.png
10 - A Test in Paradise!
/video/23-a-test-in-paradise
/static/thumbnails/23-journeys/11.png
11 - Best Friend...Worst Nightmare!
/video/23-best-friend-worst-nightmare
/static/thumbnails/23-journeys/12.png
12 - Flash of the Titans!
/video/23-flash-of-the-titans
/static/thumbnails/23-journeys/13.png
13 - The Climb to Be the Very Best!
/video/23-the-climb-to-be-the-very-best
/static/thumbnails/23-journeys/14.png
14 - Raid Battle in The Ruins!
/video/23-raid-battle-in-the-ruins
/static/thumbnails/23-journeys/15.png
15 - A Snow Day for Searching!
/video/23-a-snow-day-for-searching
/static/thumbnails/23-journeys/16.png
16 - A Chilling Curse!
/video/23-a-chilling-curse
/static/thumbnails/23-journeys/17.png
17 - Kicking It from Here Into Tomorrow!
/video/23-kicking-it-from-here-into-tomorrow
/static/thumbnails/23-journeys/18.png
18 - Destination: Coronation!
/video/23-destination-coronation
/static/thumbnails/23-journeys/19.png
19 - A Talent for Imitation!
/video/23-a-talent-for-imitation
/static/thumbnails/23-journeys/20.png
20 - Dreams Are Made of These!
/video/23-dreams-are-made-of-these
/static/thumbnails/23-journeys/21.png
21 - Caring for a Mystery!
/video/23-caring-for-a-mystery
/static/thumbnails/23-journeys/22.png
22 - Goodbye, Friend!
/video/23-goodbye-friend
/static/thumbnails/23-journeys/23.png
23 - Panic in the Park!
/video/23-panic-in-the-park
/static/thumbnails/23-journeys/24.png
24 - A Little Rocket R & R!
/video/23-a-little-rocket-r-and-r
/static/thumbnails/23-journeys/25.png
25 - A Festival Reunion!
/video/23-a-festival-reunion
/static/thumbnails/23-journeys/26.png
26 - Splash, Dash, and Smash for the Crown!
/video/23-splash-dash-smash-for-crown
/static/thumbnails/23-journeys/27.png
27 - Toughing It Out!
/video/23-toughing-it-out
/static/thumbnails/23-journeys/28.png
28 - Sobbing Sobble!
/video/23-sobbing-sobble
/static/thumbnails/23-journeys/29.png
29 - There's a New Kid in Town!
/video/23-theres-new-kid-in-town
/static/thumbnails/23-journeys/30.png
30 - Betrayed, Bothered, and Beleaguered!
/video/23-betrayed-bothered-beleaguered
/static/thumbnails/23-journeys/31.png
31 - The Cuteness Quotient!
/video/23-cuteness-quotient
/static/thumbnails/23-journeys/32.png
32 - Time After Time!
/video/23-time-after-time
/static/thumbnails/23-journeys/33.png
33 - Trade, Borrow, and Steal!
/video/23-trade-borrow-steal
/static/thumbnails/23-journeys/34.png
34 - Solitary and Menacing!
/video/23-solitary-and-menacing
/static/thumbnails/23-journeys/35.png
35 - Gotta Catch a What?!
/video/23-gotta-catch-a-what
/static/thumbnails/23-journeys/36.png
36 - Making Battles in the Sand!
/video/23-making-battles-in-the-sand
/static/thumbnails/23-journeys/37.png
37 - That New Old Gang of Mine!
/video/23-that-new-old-gang-of-mine
/static/thumbnails/23-journeys/38.png
38 - Restore and Renew!
/video/23-restore-and-renew
/static/thumbnails/23-journeys/39.png
39 - Octo-Gridlock at the Gym!
/video/23-octo-gridlock-at-the-gym
/static/thumbnails/23-journeys/40.png
40 - A Crackling Raid Battle!
/video/23-a-crackling-raid-battle
/static/thumbnails/23-journeys/41.png
41 - Pikachu Translation Check... Up to Your Neck!
/video/23-pikachu-translation-check-up-to-your-neck
/static/thumbnails/23-journeys/42.png
42 - Sword and Shield, Slumbering Weald!
/video/23-sword-and-shield-slumbering-weald
/static/thumbnails/23-journeys/43.png
43 - Sword and Shield: The Darkest Day!
/video/23-sword-and-shield-the-darkest-day
/static/thumbnails/23-journeys/44.png
44 - Sword and Shield: "From Here to Eternatus!"
/video/23-sword-and-shield-from-here-to-eternatus
/static/thumbnails/23-journeys/45.png
45 - Sword and Shield... The Legends Awaken!
/video/23-sword-and-shield-the-legends-awaken
/static/thumbnails/23-journeys/46.png
46 - Getting More Than You Battled For!
/video/23-getting-more-than-you-battled-for
/static/thumbnails/23-journeys/47.png
47 - Crowning the Chow Crusher!
/video/23-crowning-the-chow-crusher
/static/thumbnails/23-journeys/48.png
48 - A Close Call... Practically!
/video/23-a-close-call-practically
'''

Master_Journeys = '''
/static/thumbnails/24-master-journeys/1.png
01 - To Train, or Not to Train!
https://www.4shared.com/video/cyOpOIM2iq/Sword__Shield_Episode_49.html
/static/thumbnails/24-master-journeys/2.png
02 - A Pinch of This, a Pinch of That!
https://www.4shared.com/video/QQ8fOhKgiq/50_online.html
/static/thumbnails/24-master-journeys/3.png
03 - Trials of a Budding Master!
https://www.4shared.com/video/U3-7hKroiq/51_online.html
/static/thumbnails/24-master-journeys/4.png
04 - How Are You Gonna Keep 'Em Off of the Farm?
https://www.4shared.com/video/ygHyJWyvea/52_online.html
/static/thumbnails/24-master-journeys/5.png
05 - Healing the Healer!
https://www.4shared.com/video/gYDBRFzhea/53_online.html
/static/thumbnails/24-master-journeys/6.png
06 - Sobble Spies a Stealthy Strategy!
https://www.4shared.com/video/Rc0ik1eFea/54_online.html
/static/thumbnails/24-master-journeys/7.png
07 - The Tale of You and Glimwood Tangle!
https://www.4shared.com/video/21FmcSfniq/55_online.html
/static/thumbnails/24-master-journeys/8.png
08 - Searching for Chivalry!
https://www.4shared.com/video/N71tFLxKea/56_online.html
/static/thumbnails/24-master-journeys/9.png
09 - Memories of a Warming Kindness!
https://www.4shared.com/video/RAw8JTrLiq/57_online.html
/static/thumbnails/24-master-journeys/10.png
10 - A Rollicking Roll_ Eyes on the Goal!
https://www.4shared.com/video/hGCobp28ea/58_online.html
/static/thumbnails/24-master-journeys/11.png
11 - When a House is Not a Home!
https://www.4shared.com/video/kl5sgwhUiq/59_online.html
/static/thumbnails/24-master-journeys/12.png
12 - Beyond Chivalry_ Aiming to be a Leek Master!
https://www.4shared.com/video/ZxPAfs5Tea/60_online.html
/static/thumbnails/24-master-journeys/13.png
13 - Searching for Service with a Smile!
https://www.4shared.com/video/3oDr9-27ea/61_online.html
/static/thumbnails/24-master-journeys/14.png
14 - Not Too Close for Comfort!
https://www.4shared.com/video/poN1tbqHiq/62_online.html
/static/thumbnails/24-master-journeys/15.png
15 - On Land, In the Sea, and to the Future!
https://www.4shared.com/video/tHHZZJFjea/63_online.html

https://pokemon.animepisode.com/wp-content/uploads/2021/04/JN064-765x500.jpg
16 - Absol Absolved!
https://www.4shared.com/video/-BClEEf0ea/64_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/05/JN065-765x500.jpg
17 - Thrash of the Titans!
https://www.4shared.com/video/Xj-s3wx6ea/65_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/05/JN065-765x500.jpg
18 - Under Color of Darkness!
https://www.4shared.com/video/Ye9HIgl1ea/66_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/05/JN066-765x500.jpg
19 - Sleuths for Truth!
https://www.4shared.com/video/LUAr05MPea/67_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/05/JN068-765x500.jpg
20 - Advice to Goh!
https://www.4shared.com/video/ZKrpEBgeea/68_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/06/JN069-765x500.jpg
21 - Errand Endurance!
https://www.4shared.com/video/6sJG1V6jea/69_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/06/JN070-765x500.jpg
22 - Take My Thief! Please!
https://www.4shared.com/video/dnEQHk90iq/70_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/06/JN071-765x500.jpg
23 - Leaping Toward the Dream!
https://www.4shared.com/video/OmG2XoW4iq/71_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/06/JN072-765x500.jpg
24 - Everybody's Doing the Underground Shuffle!
https://www.4shared.com/video/TU2QqqTqiq/72_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/07/JN073-765x500.jpg
25 - Grabbing the Brass Ring!
https://www.4shared.com/video/rPx9PdYgea/73_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/07/JN074-765x500.jpg
26 - Nightfall? Nightmares!
https://www.4shared.com/video/lygMds4Sea/74_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/07/JN075-765x500.jpg
27 - A Midsummer Night's Light!
https://www.4shared.com/video/dSp19QE7ea/75_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/08/JN076-765x500.jpg
28 - All Out, All of the Time!
https://www.4shared.com/video/1NVZ-wf7iq/76_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/08/JN077-765x500.jpg
29 - Excitement from the Ultra-Shocking Start!
https://www.4shared.com/video/x8mMZThOiq/77_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/08/JN078-765x500.jpg
30 - Detective Drizzile!
https://www.4shared.com/video/ChT7omToea/78_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/09/JN079-765x500.jpg
31 - Night and Day, You Are the Ones!
https://www.4shared.com/video/CnhV5akzea/79_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/09/JN080-765x500.jpg
32 - Trial on a Golden Scale!
https://www.4shared.com/video/JRT0JpTdea/80_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/09/JN081-765x500.jpg
33 - Mad About Blue!
https://www.4shared.com/video/P88QMc5Ziq/81_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/10/JN082-765x500.jpg
34 - The Sweet Taste of Battle!
https://www.4shared.com/video/iPkLQ5OKiq/82_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/10/JN083-765x500.jpg
35 - Star Night, Star Fright!
https://www.4shared.com/video/jak6yb8Fiq/83_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/10/JN084-765x500.jpg
36 - An Adventure of Mega Proportions!
https://www.4shared.com/video/fzUr3tOhiq/84_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/10/JN085-765x500.jpg
37 - Battle Three with Bea!
https://www.4shared.com/video/EL_WZka2iq/85_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/11/JN086-765x500.jpg
38 - A Battle of Mega Versus Max!
https://www.4shared.com/video/Xj-pAtG_iq/86_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/11/JN087-765x500.jpg
39 - Breaking the Ice!
https://www.4shared.com/video/vWpc6eaxiq/87_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/11/JN088-765x500.jpg
40 - Looking Out for Number Two!
https://www.4shared.com/video/8LpwuZO9ea/88_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/12/JN089-765x500.jpg
41 - The Gates of Warp!
https://www.4shared.com/video/DPnCvDrDiq/89_online.html
https://pokemon.animepisode.com/wp-content/uploads/2021/12/JN090-765x500.jpg
42 - Showdown at the Gates of Warp!
https://www.4shared.com/video/AYBWOo5Xiq/90_online.html
'''
