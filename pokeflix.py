import os
import re
import os.path
import sys
import platform
from pokeflix_data import *
from configparser import ConfigParser
from pathlib import Path
import subprocess
import json
config = ConfigParser()
config.read('pokeflix.conf')
website = config.get('CONFIG','website')
downloader = config.get('CONFIG','downloader')
main_folder = config.get('CONFIG','main_folder')
use_player = bool(int(config.get('CONFIG','use_player')))
player = config.get('CONFIG','player')

if platform.system() == "Windows":
    slash = "\\"
else:
    slash = "/"
series = series.splitlines()
movies = movies.splitlines()
specials = specials.splitlines()
thumbnail_urls = ("https://pokemon.animepisode.com/wp-content/uploads/","https://assets.pokemon.com/assets/cms2/img/watch-pokemon-tv/original")
def help():
    print("""- -s or --series Download a series
- -e or --episode Download or play an episode of a pokemon series
- -m or --movies Download or play a movie
- -am or --all-movies Download ALL movies
- -S or --specials Download a special
- -aS or --all-specials Download ALL specials
- -g or --generation Download a whole generation all of it's series
- -dp or --dirplay to help you pick from an ordered list an episode you downloaded to play in your player.
- -f or --find to search movies and series for some string""")
if len(sys.argv) == 1:
    print("An incorrect or no argument was given")
    help()
elif sys.argv[1] == "-s" or sys.argv[1] == "--series":
    for i, gen in enumerate(series):
        if gen.startswith("Generation "):
            print(gen)
    try:
        genpick = int(input("Generation number: "))
    except:
        print("ERROR: Needs to be an ineger")
        quit()
    if genpick <= 0 or genpick > 8:
        print("ERROR: Has to be a number fom 1-8")
        quit()
    else:
        genpick = str(genpick)
        for i, serie in enumerate(series):
            if serie.startswith("("+genpick+")"):
                genserie = serie.replace("("+genpick+") ","")
                print(genserie)
                genserieindex = series.index(serie)+1
    seriepick = int(input("Series number: "))
    if seriepick <= 0 or seriepick > 4:
        print("ERROR: Has to be number from 1-4")
        quit()
    num = genserieindex-seriepick
    final = series[num].replace('('+genpick+') ','')
    print(f"Picking: {final.replace('Pokemon: ','')}")
    final = final.replace("Pokemon: ","").replace(" & ","_").replace(" ","_").replace(":","")

    if final in locals():
        videos = globals()[final]
        videos = videos.splitlines()
    else:
        print("ERROR: This season isn't in the database yet...")
        quit()

    #folder = os.path.join(main_folder,final.replace("_","-").lower())
    folder = main_folder+final.replace("_","-").lower()+slash
    print(f"Will be downloading to {folder}")
    if os.path.exists(folder):
        pass
    else:
        os.mkdir(folder)
    yesno = input("Try to get thumbnails needs requests (N/y): ")
    if yesno == "yes" or yesno == "Y" or yesno == "y":
        get_thumb = True
    else:
        get_thumb = False
    if get_thumb == True:
        import requests
        if os.path.exists(folder+"thumbnails"):
            pass
        else:
            os.mkdir(folder+"thumbnails")
    name = 0
    for video in videos:
        if video.startswith("/static"):
            if get_thumb == True:
                thumbnail_data = requests.get(website+video)
                name = folder+"thumbnails"+slash+video.split("/")[4]
                with open(name,"wb") as f:
                    f.write(thumbnail_data.content)
                print("GETTING THUMBNAILS")
        elif video.startswith(thumbnail_urls):
            if get_thumb == True:
                print(video)
                thumbnail_data = requests.get(video)
                name = folder+"thumbnails"+slash+video.split("/")[-1]
                print(name)
                #name = folder+"thumbnails"+slash+video.split("/")[4]
                with open(name,"wb") as f:
                    f.write(thumbnail_data.content)
                print("GETTING THUMBNAILS")
        elif video.startswith("/video"):
            print("cd {folder} && {downloader} {website+video} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")
            os.system(f"cd {folder} && {downloader} {website+video} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")
        elif video.startswith("https://www.4shared.com/video/"):
            os.system(f"cd {folder} && {downloader} {video} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")
        else:
            print(video)
elif sys.argv[1] == "-g" or sys.argv[1] == "--generation":
    for i, gen in enumerate(series):
        if gen.startswith("Generation "):
            print(gen)
    try:
        genpick = int(input("Generation number: "))
    except:
        print("ERROR: Needs to be an ineger")
        quit()
    if genpick <= 0 or genpick > 8:
        print("ERROR: Has to be a number fom 1-8")
        quit()
    else:
        genpick = str(genpick)
        print(f"Will be downloading all seasons in this generation to {main_folder}")
        yesno = input("Try to get thumbnails needs requests (N/y): ")
        for serie in reversed(series):
            if serie.startswith("("+genpick+")"):
                genserie = serie.replace("("+genpick+") ","").replace("Pokemon: ","")
                print(genserie)
                final = series[series.index(serie)].replace('('+genpick+') ','')
                final = final.replace("Pokemon: ","").replace(" & ","_").replace(" ","_").replace(":","")            
                if final in locals():
                    videos = globals()[final]
                    videos = videos.splitlines()
                else:
                    print("ERROR: This season isn't in the database yet...")
                    quit()
                folder = main_folder+final.replace("_","-").lower()+slash
                print(f"Will be downloading to {folder}")
                if os.path.exists(folder):
                    pass
                else:
                    os.mkdir(folder)
                    
                if yesno == "yes" or yesno == "Y" or yesno == "y":
                    get_thumb = True
                else:
                    get_thumb = False
                if get_thumb == True:
                    import requests
                    if os.path.exists(folder+"thumbnails"):
                        pass
                    else:
                        os.mkdir(folder+"thumbnails")
            
                for video in videos:
                    if video.startswith("/static"):
                        if get_thumb == True:
                            thumbnail_data = requests.get(website+video)
                            name = folder+"thumbnails"+slash+video.split("/")[4]
                            with open(name,"wb") as f:
                                f.write(thumbnail_data.content)
                            print("GETTING THUMBNAILS")
                    elif video.startswith("/video"):
                        os.system(f"cd {folder} && {downloader} {website+video} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")
                    else:
                        print(video)
                print(f"Finished downloading {final}")

elif sys.argv[1] == "-e" or sys.argv[1] == "--episode":
    for i, gen in enumerate(series):
        if gen.startswith("Generation "):
            print(gen)
    try:
        genpick = int(input("Generation number: "))
    except:
        print("ERROR: Needs to be an ineger")
        quit()
    if genpick <= 0 or genpick > 8:
        print("ERROR: Has to be a number fom 1-8")
        quit()
    else:
        genpick = str(genpick)
        for i, serie in enumerate(series):
            if serie.startswith("("+genpick+")"):
                genserie = serie.replace("("+genpick+") ","")
                print(genserie)
                genserieindex = series.index(serie)+1
    seriepick = int(input("Series number: "))
    if seriepick <= 0 or seriepick > 4:
        print("ERROR: Has to be number from 1-4")
        quit()
    num = genserieindex-seriepick
    final = series[num].replace('('+genpick+') ','')
    print(f"Picking: {final.replace('Pokemon: ','')}")
    final = final.replace("Pokemon: ","").replace(" & ","_").replace(" ","_").replace(":","")

    if final in locals():
        videos = globals()[final]
        videos = videos.splitlines()
    else:
        print("ERROR: This season isn't in the database yet...")
        quit()
    videos = globals()[final]
    videos = videos.splitlines()
    for video in videos:
        if video.startswith("/static"):
            pass
        elif video.startswith("/video"):
            pass
        else:
            print(video)
    print(f"Select the number of the video you want to download. e.g 1-100.")
    try:
        episode = int(input("Episode number: "))
    except:
        print("ERROR: Needs to be an integer")
        quit()
    if episode in list(range(9)):
        episode = "0"+str(episode)
    for video in videos:
        if video.startswith(str(episode)):
            if use_player == True:
                print(f"Playing {video}")
                os.system(f"{player} {website+videos[videos.index(video)+1]}")
            else:
                print(f"Downloading {video}. Will be downloading to {main_folder}")
                os.system(f"cd {main_folder} && {downloader} {website+videos[videos.index(video)+1]} --output \"{videos[videos.index(video)]}.%(ext)s\"")
elif sys.argv[1] == "-m" or sys.argv[1] == "--movies":
    for i, gen in enumerate(movies):
        if gen.startswith("Generation "):
            print(gen)
    try:
        genpick = int(input("Generation number: "))
    except:
        print("ERROR: Needs to be an ineger")
        quit()
    if genpick <= 0 or genpick > 8:
        print("ERROR: Has to be a number fom 1-8")
        quit()
    else:
        genpick = str(genpick)
        for i, serie in enumerate(movies):
            if serie.startswith("("+genpick+")"):
                genserie = serie.replace("("+genpick+") ","")
                print(genserie)
                genserieindex = movies.index(serie)+1
    moviepick = int(input("Movie number: "))
    if moviepick <= 0 or moviepick > 4:
        print("ERROR: Has to be number from 1-4")
        quit()
    if moviepick == 1:
        link = movies[genserieindex]
    else:
        link = movies[genserieindex-((moviepick-1)*3)]
    #num = genserieindex-moviepick
    #final = movies[num].replace('('+genpick+') ','')

    for video in movies:
        if video == link:
            if use_player == True:
                print(f"Playing {website+link}")
                os.system(f"{player} {website+link}")
            else:
                print(f"Downloading {website+link} to {main_folder}")
                os.system(f"cd {main_folder} && {downloader} {website+link} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")

elif sys.argv[1] == "-S" or sys.argv[1] == "--specials":
    for i, special in enumerate(specials):
        print(i, special)
    try:
        pick = int(input("Special: "))
    except:
        print("ERROR: Needs to be an integer")
        quit()
    final = specials[pick]
    print(f"Picking: {final}")
    final = final.replace(" ","_")

    if final in locals():
        videos = globals()[final]
        videos = videos.splitlines()
    else:
        print("ERROR: This special isn't in the database yet...")
        quit()

    folder = main_folder+final.replace("_","-").lower()+slash
    print(f"Will be downloading to {folder}")
    if os.path.exists(folder):
        pass
    else:
        os.mkdir(folder)
    yesno = input("Try to get thumbnails needs requests (N/y): ")
    if yesno == "yes" or yesno == "Y" or yesno == "y":
        get_thumb = True
    else:
        get_thumb = False
    if get_thumb == True:
        import requests
        if os.path.exists(folder+"thumbnails"):
            pass
        else:
            os.mkdir(folder+"thumbnails")

    for i, video in enumerate(videos):
        if video.startswith("/static"):
            if get_thumb == True:
                thumbnail_data = requests.get(website+video)
                name = folder+"thumbnails"+slash+video.split("/")[4]
                with open(name,"wb") as f:
                    f.write(thumbnail_data.content)
                print("GETTING THUMBNAILS")
        elif video.startswith(thumbnail_urls):
            if get_thumb == True:
                thumbnail_data = requests.get(video)
                name = folder+"thumbnails"+slash+video.split("_")[-1]
                with open(name,"wb") as f:
                    f.write(thumbnail_data.content)
                print("GETTING THUMBNAILS")
                
        elif video.startswith("/video"):
            os.system(f"cd {folder} && {downloader} {website+video} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")
        elif video.startswith("https://watch.pokemon.com/en-us/#/player?id="):
            os.system(f"cd {folder} && {downloader} {video} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")
            
        else:
            print(video)
elif sys.argv[1] == "-dp" or sys.argv[1] == "--dirplay":
    dirs = os.listdir(main_folder)
    for i, dir in enumerate(dirs):
        print(i, main_folder+dir)
    dirpath = int(input("Which directory? "))
    paths = sorted(Path(main_folder+dirs[dirpath]).iterdir(), key=os.path.getmtime)
    for i, path in enumerate(paths):
        print(i, path)

    pick = int(input("Which one do you want to play? "))
    play = paths[pick]
    print(f"Playing {play}")
    os.system(f"{player} \"{play}\"")
elif sys.argv[1] == "-f" or sys.argv[1] == "--find":
    query = ""
    query = sys.argv[2:]
    query = ' '.join(query)
    if query == "":
        while not query:
            query = input("Searching for: ")
    query = str(query)
    print("SERIES:")
    for serie in reversed(series):
        final = re.sub('\((.+?)\) ','',series[series.index(serie)])
        final = final.replace("Pokemon: ","").replace(" & ","_").replace(" ","_").replace(":","")            
        if final in locals():
            videos = globals()[final]
            videos = videos.splitlines()
        for video in videos:
            if video.startswith("/static"):
                pass
            elif video.startswith("/video"):
                pass
            else:
                if query.lower() in video.lower():
                    print(video)
    print("\nMOVIES:")
    for video in movies:
        if video.startswith("/static"):
            pass
        elif video.startswith("/v"):
            pass
        elif video.startswith("Generation"):
            pass
        else:
            if query.lower() in video.lower():
                print(re.sub('\((.+?)\) ','',video))

elif sys.argv[1] == "-am" or sys.argv[1] == "--all-movies":
    for video in movies:
        if video.startswith("/v/"):
            print(f"Downloading {website+video} to {main_folder}")
            os.system(f"cd {main_folder} && {downloader} {website+video} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")
elif sys.argv[1] == "-aS" or sys.argv[1] == "--all-specials":
    yesno = input("Try to get thumbnails needs requests (N/y): ")
    if yesno == "yes" or yesno == "Y" or yesno == "y":
        get_thumb = True
    else:
        get_thumb = False
    for i, special in enumerate(specials):
        print(i,special)
        special = special.replace(" ","_")
        if special in locals():
            videos = globals()[special]
            videos = videos.splitlines()
        else:
            print("ERROR: This special isn't in the database yet...")
            quit()
        folder = main_folder+special.replace("_","-").lower()+slash
        print(f"Will be downloading to {folder}")
        if os.path.exists(folder):
            pass
        else:
            os.mkdir(folder)
        if get_thumb == True:
            import requests
            if os.path.exists(folder+"thumbnails"):
                pass
            else:
                os.mkdir(folder+"thumbnails")

        for i, video in enumerate(videos):
            if video.startswith("/static"):
                if get_thumb == True:
                    thumbnail_data = requests.get(website+video)
                    name = folder+"thumbnails"+slash+video.split("/")[4]
                    with open(name,"wb") as f:
                        f.write(thumbnail_data.content)
                    print("GETTING THUMBNAILS")
            elif video.startswith(thumbnail_urls):
                if get_thumb == True:
                    thumbnail_data = requests.get(video)
                    #name = str(i+1)+".png"
                    name = folder+"thumbnails"+slash+video.split("_")[-1]
                    with open(name,"wb") as f:
                        f.write(thumbnail_data.content)
                    print("GETTING THUMBNAILS")

            elif video.startswith("/video"):
                os.system(f"cd {folder} && {downloader} {website+video} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")
            elif video.startswith("https://watch.pokemon.com/en-us/#/player?id="):
                os.system(f"cd {folder} && {downloader} {video} --output \"{videos[videos.index(video)-1]}.%(ext)s\"")

elif sys.argv[1] == "-dp" or sys.argv[1] == "--dirplay":
    dirs = os.listdir(main_folder)
    for i, dir in enumerate(dirs):
        print(i, main_folder+dir)
    dirpath = int(input("Which directory? "))
    paths = sorted(Path(main_folder+dirs[dirpath]).iterdir(), key=os.path.getmtime)
    for i, path in enumerate(paths):
        print(i, path)

    pick = int(input("Which one do you want to play? "))
    play = paths[pick]
    print(f"Playing {play}")
    os.system(f"{player} \"{play}\"")
elif sys.argv[1] == "-f" or sys.argv[1] == "--find":
    query = ""
    query = sys.argv[2:]
    query = ' '.join(query)
    if query == "":
        while not query:
            query = input("Searching for: ")
    query = str(query)
    print("SERIES:")
    for serie in reversed(series):
        final = re.sub('\((.+?)\) ','',series[series.index(serie)])
        final = final.replace("Pokemon: ","").replace(" & ","_").replace(" ","_").replace(":","")            
        if final in locals():
            videos = globals()[final]
            videos = videos.splitlines()
        for video in videos:
            if video.startswith("/static"):
                pass
            elif video.startswith("/video"):
                pass
            else:
                if query.lower() in video.lower():
                    print(video)
    print("\nMOVIES:")
    for video in movies:
        if video.startswith("/static"):
            pass
        elif video.startswith("/v"):
            pass
        elif video.startswith("Generation"):
            pass
        else:
            if query.lower() in video.lower():
                print(re.sub('\((.+?)\) ','',video))

            
elif sys.argv[1] == "-h" or sys.argv[1] == "--help":
    help()
    quit()
else:
    print("An incorrect or no argument was given")
    help()
    quit()

