import requests
import re
import html

def scrape(url):
    data = requests.get(url).text
    find = re.findall(f'<p><a href="(.*?)" class="btn btn-default"', data)
    links = []
    for link in find:
        link = link
        links.append(f'https://pokeflix.tv{link}')

    find = re.findall(f'<h4>(.*?)</h4>', data)
    names = []
    for name in find:
        names.append(html.unescape(name))

    find = re.findall(f'<img src="(.*?)" alt="thumbnail" />', data)
    images = []
    for image in find:
        images.append(f'https://pokeflix.tv{image}')

    dict = [
        {'name': names, 'links': links, "images": images}
        for names, links, images in zip(names, links, images)
    ]
    
    return dict

def get_episodes(link):
    return scrape(link)

def get_seasons():
    return scrape("https://pokeflix.tv")

def get_movies():
    return scrape("https://pokeflix.tv/movies/")

def get_specials():
    return scrape("https://pokeflix.tv/specials/")

#print(get_episodes("https://www.pokeflix.tv/browse/pokemon-indigo-league"))
#def get_seasons():
    
data = [
    {"gen": "Generation 1 (Kanto Region & Orange Islands)",
     "seasons": [
         {"name": "Orange Island Adventures",
          "link": "https://pokeflix.tv/browse/pokemon-orange-island-adventures"},
         {"name": "Indigo League",
          "link": "https://pokeflix.tv/browse/pokemon-indigo-league"},
     ],
     },
    {"gen": "Generation 2 (Johto Region)",
     "seasons": [
         {"name": "Master Quest",
          "link": "https://pokeflix.tv/browse/pokemon-master-quest"},
         {"name": "Johto League Champions",
          "link": "https://pokeflix.tv/browse/pokemon-johto-league-champions"},
         {"name": "The Johto Journeys",
          "link": "https://pokeflix.tv/browse/pokemon-the-johto-journeys"},
     ],
     },
    {"gen": "Generation 3 (Hoenn Region)",
     "seasons": [
         {"name": "Battle Frontier",
          "link": "https://pokeflix.tv/browse/pokemon-battle-frontier"},
         {"name": "Advanced Battle",
          "link": "https://pokeflix.tv/browse/pokemon-advanced-battle"},
         {"name": "Advanced Challenge",
          "link": "https://pokeflix.tv/browse/pokemon-advanced"},
     ],
     },
    {"gen": "Generation 4 (Sinnoh Region)",
     "seasons": [
         {"name": "DP Sinnoh League Victors",
          "link": "https://pokeflix.tv/browse/pokemon-dp-sinnoh-league-victors"},
         {"name": "DP Galactic Battles",
          "link": "https://pokeflix.tv/browse/pokemon-dp-galactic-battles"},
         {"name": "DP Battle Dimension",
          "link": "https://pokeflix.tv/browse/pokemon-dp-battle-dimension"},
         {"name": "Diamond and Pearl",
          "link": "https://pokeflix.tv/browse/pokemon-diamond-and-pearl"},
     ],
     },
    {"gen": "Generation 5 (Unova Region)",
     "seasons": [
         {"name": "BW Adventures in Unova",
          "link": "https://pokeflix.tv/browse/pokemon-bw-adventures-in-unova"},
         {"name": "BW Rival Destinies",
          "link": "https://pokeflix.tv/browse/pokemon-bw-rival-destinies"},
         {"name": "Black & White",
          "link": "https://pokeflix.tv/browse/pokemon-black-and-white"},
     ],
     },
    {"gen": "Generation 6 (Kalos Region)",
     "seasons": [
         {"name": "XYZ",
          "link": "https://pokeflix.tv/browse/pokemon-xyz"},
         {"name": "XY Kalos Quest",
          "link": "https://pokeflix.tv/browse/pokemon-xy-kalos-quest"},
         {"name": "XY",
          "link": "https://pokeflix.tv/browse/pokemon-xy"},
     ],
     },
    {"gen": "Generation 7 (Alola Region)",
     "seasons": [
         {"name": "Sun & Moon: Ultra Lengends",
          "link": "https://pokeflix.tv/browse/pokemon-sun-and-moon-ultra-legends"},
         {"name": "Sun & Moon: Ultra Adventures",
          "link": "https://pokeflix.tv/browse/pokemon-sun-and-moon-ultra-adventures"},
         {"name": "Sun & Moon",
          "link": "https://pokeflix.tv/browse/pokemon-sun-and-moon"},
     ],
     },
    {"gen": "Generation 8 (Galar Region)",
     "seasons": [
         {"name": "Journeys",
          "link": "https://pokeflix.py/browse/pokemon-journeys"}
         ],
     },
]

for season in data[6]["seasons"]:
    print(season["name"])
